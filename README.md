# GrowBike

## The final solution, hosted on a VPS, can be found at: https://growbike.net/

## SETUP

### DISCLAIMER: All of the following steps should be run on the server.

### Download all the city data from Dropbox and place it in the directory /BicycleData/dataForWrangling. You should have a folder for each city before proceeding to the next step.

### CD into /BicycleData/utilities

### First run the python script "createDirectories.py" - using python3 createDirectories.py

#### This should create all the necessary folders in a directory called "/BycicleData/dataReady"

### Secondly run the python script "createDirectoriesPublic.py" - follow the same approach as in step 1.

#### This should create all the necessary folders in a /nextjs/public/ - check to see if there is a /nextjs/public/Videos and /nextjs/public/Images directory with all the cities presents.

### Thirdly run the python script "moveVideosImages.py"

### Fourthly run the python script "newDataWrangler.py" - this will format and place the network data and relevant .csv files in the correct directories. OBS! This might take some time.

### With all the data correctly formated and placed in the relevant directories, it is now time to publiate the database

### First we make sure the database is running.

#### Try running the database by writing "mongo" in the terminal - if this starts mongo we are good to go.

#### If the database did not start, run "sudo systemctl start mongod" - follow this by the previous step. If this does not work follow the instructions at https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

### Now back to publiating the database

### CD into /nextjs/database

### Run the "insertAll.js" script by writing "node insertAll.js" - if this command fails check your ".env.local" file. The url in the "insertAll.js" file and database string should match those of your ".env.local" file.

### Now the database should be publiated - this can be checked by starting the database running "mongo" and using the command "show dbs" to check if the correct database is publiated with data.

### Now everything is in place to run the application.

### CD into /nextjs/

### Run "yarn build"

### Run "nohup yarn start" - if a process with yarn start is already running, run "killall node" and try again. This should kill all previously running node processes.

## The application is now running at "URL"
