export function addSourcesAndLayers(map) {
  map.addSource("poi", {
    type: "geojson",
    data: null,
  });
  map.addLayer({
    id: "pointOfInterest",
    source: "poi",
    type: "symbol",
    layout: {
      "icon-image": "rail",
      "icon-padding": 0.0,
      "icon-allow-overlap": true,
      "icon-size": ["interpolate", ["linear"], ["zoom"], 7, 0.5, 15, 1],
    },
  });
}

export function addGridPOI(map) {
  map.addSource("gridPoi", {
    type: "geojson",
    data: null,
  });
  map.addLayer({
    id: "pointOfInterestGrid",
    source: "gridPoi",
    type: "circle",
    paint: {
      "circle-radius": 5,
      "circle-color": "#0b8fa6",
      "circle-stroke-color": "#0b8fa6",
      "circle-stroke-width": 1,
      "circle-opacity": 1,
    },
  });
}

export function addPathLayer(map) {
  map.addSource("bicyclepaths", {
    type: "geojson",
    data: null,
    tolerance: 0.15,
  });
  map.addLayer({
    id: "paths",
    source: "bicyclepaths",
    type: "line",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#0eb6d2",
      "line-width": 3.75,
    },
  });
}

export function addExistingLayer(map) {
  map.addSource("existingnetwork", {
    type: "geojson",
    data: null,
  });
  map.addLayer({
    id: "existingpaths",
    source: "existingnetwork",
    type: "line",
    layout: {
      "line-join": "round",
      "line-cap": "round",
    },
    paint: {
      "line-color": "#2222ff",
      "line-width": 1.25,
    },
  });
}
