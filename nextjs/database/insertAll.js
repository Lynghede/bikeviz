const fs = require("fs");
const { MongoClient } = require("mongodb");

// Replace the following with your Atlas connection string
// const url =
//   "mongodb+srv://moly:108391vest@cluster0.p2oqg.mongodb.net/bikevizatlas?retryWrites=true&w=majority";
const url = "mongodb://localhost:27017/newAllData";
const client = new MongoClient(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
// The database to use
const dbName = "newAllData";

const ids = ['london', 'tokyo', 'santiago', 'glasgow', 'rome', 'leeds', 'philadelphia', 'delft', 'montreal', 'rabat', 'mumbai', 'munich', 'moscow', 'ulaanbaatar', 'malmo', 'tashkent', 'sheffield', 'warsaw', 'bath', 'phoenix', 'birmingham', 'paris', 'hamburg', 'manchestergreater', 'edinburgh', 'sanfrancisco', 'saopaulo', 'luanda', 'boston', 'detroit', 'copenhagen', 'hongkong', 'kathmandu', 'manhattan', 'milan', 'turin', 'jakarta', 'vienna', 'shahalam', 'karachi', 'lyon', 'stuttgart', 'barcelona', 'singapore', 'buenosaires', 'telaviv', 'bern', 'toronto', 'budapest', 'losangeles', 'oslo', 'marrakesh', 'houston', 'bradford', 'cologne', 'mexico', 'chicago', 'berlin', 'bogota', 'zurich', 'helsinki', 'amsterdam']

// List of cities with no rail data
// Kathmandu
// Rabat
// marrakesh
// shahalam
// bogota

const growthTypes = ["bq", "rq", "cq"];

async function getCityDataFS(id) {


  const responseExisting = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/existing/${id}_biketrack.json`,
    "utf8"
  );

  const responseExistingStats = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/existing/${id}_existing.json`,
    "utf8"
  );

  const responsePoiGrid = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/poi/${id}_poi_grid.json`,
    "utf8"
  );

  const responsePoiRail = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/poi/${id}_poi_rail.json`,
    "utf8"
  );


  const existingBicycle = JSON.parse(responseExisting);
  const existingStats = JSON.parse(responseExistingStats);
  const poiRail = JSON.parse(responsePoiRail);
  const poiGrid = JSON.parse(responsePoiGrid);
  //pass on all the data here
  return {
    id: id,
    existingBicycle,
    existingStats,
    poiRail,
    poiGrid,
  };
}

async function getStatisticsData(id) {
  const railBq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsRail/bq/${id}_stats_bq.json`,
    "utf8"
  );

  const railCq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsRail/cq/${id}_stats_cq.json`,
    "utf8"
  );

  const railRq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsRail/rq/${id}_stats_rq.json`,
    "utf8"
  );
  const gridBq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsGrid/bq/${id}_stats_bq.json`,
    "utf8"
  );

  const gridCq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsGrid/cq/${id}_stats_cq.json`,
    "utf8"
  );

  const gridRq = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/statsGrid/rq/${id}_stats_rq.json`,
    "utf8"
  );

  const statsRailBq = JSON.parse(railBq);
  const statsRailCq = JSON.parse(railCq);
  const statsRailRq = JSON.parse(railRq);
  const statsGridBq = JSON.parse(gridBq);
  const statsGridCq = JSON.parse(gridCq);
  const statsGridRq = JSON.parse(gridRq);

  return {
    id: `StatisticsSyntheticNetworks`,
    statsRailBq,
    statsRailCq,
    statsRailRq,
    statsGridBq,
    statsGridCq,
    statsGridRq,
  };
}

async function getGridNetworkFS(id, growthType) {
  const fileName = `${id}_grid_${growthType}`;
  const responsestage1 = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/grid/${growthType}/${fileName}1.json`,
    "utf8"
  );

  const responsestage2 = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/grid/${growthType}/${fileName}2.json`,
    "utf8"
  );

  const responsestage3 = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/grid/${growthType}/${fileName}3.json`,
    "utf8"
  );
  const responsestage4 = fs.readFileSync(
    `../../BicycleData/dataReady/${id}/grid/${growthType}/${fileName}4.json`,
    "utf8"
  );
  const stage1 = JSON.parse(responsestage1);
  const stage2 = JSON.parse(responsestage2);
  const stage3 = JSON.parse(responsestage3);
  const stage4 = JSON.parse(responsestage4);

  return {
    id: `bicycleGrid_${growthType}`,
    stage1,
    stage2,
    stage3,
    stage4,
  };
}

// function TMP_testjson(data, path){
//   try {
//     JSON.parse(data)

//   } catch(e) {
//     console.log("could parse ", path)
//   }
// }

function safeReadFile(path, placeholderPath) {
  try {
    const data = fs.readFileSync(
      path,
      "utf8"
    );
    // TMP_testjson(data, path)
    return data
  } catch(e) {
    console.log("a file was missing, using placeholder for " + path)
    const placeHolderData = fs.readFileSync(placeholderPath, "utf8")
    // TMP_testjson(placeHolderData, placeholderPath)
    return placeHolderData
  }
}

async function getRailwayNetworkFS(id, growthType) {
  const fileName = `${id}_rail_${growthType}`;

  const responsestage1 = safeReadFile(
    `../../BicycleData/dataReady/${id}/rail/${growthType}/${fileName}1.json`,
    `../../BicycleData/placeholder/placeholder1.json`,
  );

  const responsestage2 = safeReadFile(
    `../../BicycleData/dataReady/${id}/rail/${growthType}/${fileName}2.json`,
    `../../BicycleData/placeholder/placeholder2.json`,
  );

  const responsestage3 = safeReadFile(
    `../../BicycleData/dataReady/${id}/rail/${growthType}/${fileName}3.json`,
    `../../BicycleData/placeholder/placeholder3.json`,
  );
  const responsestage4 = safeReadFile(
    `../../BicycleData/dataReady/${id}/rail/${growthType}/${fileName}4.json`,
    `../../BicycleData/placeholder/placeholder4.json`,
  );
  const stage1 = JSON.parse(responsestage1);
  const stage2 = JSON.parse(responsestage2);
  const stage3 = JSON.parse(responsestage3);
  const stage4 = JSON.parse(responsestage4);

  return {
    id: `bicycleRail_${growthType}`,
    stage1,
    stage2,
    stage3,
    stage4,
  };
}

async function run() {
  try {
    await client.connect();
    console.log("Connected correctly to server");
    const db = client.db(dbName);

    // Use the collection "people"
    for (const id of ids) {
      const col = db.collection(id);

      let cityData = await getCityDataFS(id);
      let statisticsData = await getStatisticsData(id);
      const p = await col.insertMany([cityData, statisticsData]);

      for (const growth of growthTypes) {
        let gridStages = await getGridNetworkFS(id, growth);
        let railwayStages = await getRailwayNetworkFS(id, growth);
        const q = await col.insertMany([gridStages, railwayStages]);
      }

      // Insert a single document, wait for promise so we can read it back

      // Find one document
      const myDoc = await col.findOne();
      // Print to the console
      console.log(myDoc);
    }
  } catch (err) {
    console.log(err.stack);
  } finally {
    await client.close();
  }
}

run().catch(console.dir);
