const { MongoClient, Cursor } = require("mongodb");
//import { gridAndRailNetworks } from "../public/GrownNetworks";

const { MONGODB_URI, MONGODB_DB } = process.env;

if (!MONGODB_URI) {
  throw new Error(
    "Please define the MONGODB_URI environment variable inside .env.local"
  );
}

if (!MONGODB_DB) {
  throw new Error(
    "Please define the MONGODB_DB environment variable inside .env.local"
  );
}

/**
 * Global is used here to maintain a cached connection across hot reloads
 * in development. This prevents connections growing exponentially
 * during API Route usage.
 */
let cached = global.mongo;

if (!cached) {
  cached = global.mongo = { conn: null, promise: null };
}

export async function connectToDatabase() {
  if (cached.conn) {
    return cached.conn;
  }

  if (!cached.promise) {
    const opts = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };

    cached.promise = MongoClient.connect(MONGODB_URI, opts).then((client) => {
      return {
        client,
        db: client.db(MONGODB_DB),
      };
    });
  }
  cached.conn = await cached.promise;
  return cached.conn;
}

export async function getAllStages(id) {
  const { db } = await connectToDatabase();

  const gridAndRailNetworks = {
    gridStages_bq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
    railStages_bq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
    gridStages_rq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
    railStages_rq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
    gridStages_cq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
    railStages_cq: {
      type: "FeatureCollection",
      features: [
        [
          {
            type: "Feature",
            properties: {
              stage: 0,
            },
            geometry: {
              type: "GeometryCollection",
              geometries: [],
            },
          },
        ],
      ],
    },
  };

  // calls to the database
  const grid_bq = await db
    .collection(id)
    .find({ id: "bicycleGrid_bq" })
    .toArray();
  const grid_rq = await db
    .collection(id)
    .find({ id: "bicycleGrid_rq" })
    .toArray();
  const grid_cq = await db
    .collection(id)
    .find({ id: "bicycleGrid_cq" })
    .toArray();
  const rail_bq = await db
    .collection(id)
    .find({ id: "bicycleRail_bq" })
    .toArray();
  const rail_rq = await db
    .collection(id)
    .find({ id: "bicycleRail_rq" })
    .toArray();
  const rail_cq = await db
    .collection(id)
    .find({ id: "bicycleRail_cq" })
    .toArray();
  const additionalCityData = await db.collection(id).find({ id: id }).toArray();
  const statisticsData = await db
    .collection(id)
    .find({ id: "StatisticsSyntheticNetworks" })
    .toArray();

  // fill stages with the algorithmically grown network
  // console.log({ grid_bq });

  await gridAndRailNetworks.gridStages_bq.features.push(
    ...grid_bq[0].stage1.features
  );
  await gridAndRailNetworks.gridStages_bq.features.push(
    ...grid_bq[0].stage2.features
  );
  await gridAndRailNetworks.gridStages_bq.features.push(
    ...grid_bq[0].stage3.features
  );
  await gridAndRailNetworks.gridStages_bq.features.push(
    ...grid_bq[0].stage4.features
  );

  await gridAndRailNetworks.gridStages_rq.features.push(
    ...grid_rq[0].stage1.features
  );
  await gridAndRailNetworks.gridStages_rq.features.push(
    ...grid_rq[0].stage2.features
  );
  await gridAndRailNetworks.gridStages_rq.features.push(
    ...grid_rq[0].stage3.features
  );
  await gridAndRailNetworks.gridStages_rq.features.push(
    ...grid_rq[0].stage4.features
  );

  await gridAndRailNetworks.gridStages_cq.features.push(
    ...grid_cq[0].stage1.features
  );
  await gridAndRailNetworks.gridStages_cq.features.push(
    ...grid_cq[0].stage2.features
  );
  await gridAndRailNetworks.gridStages_cq.features.push(
    ...grid_cq[0].stage3.features
  );
  await gridAndRailNetworks.gridStages_cq.features.push(
    ...grid_cq[0].stage4.features
  );

  await gridAndRailNetworks.railStages_bq.features.push(
    ...rail_bq[0].stage1.features
  );
  await gridAndRailNetworks.railStages_bq.features.push(
    ...rail_bq[0].stage2.features
  );
  await gridAndRailNetworks.railStages_bq.features.push(
    ...rail_bq[0].stage3.features
  );
  await gridAndRailNetworks.railStages_bq.features.push(
    ...rail_bq[0].stage4.features
  );

  await gridAndRailNetworks.railStages_rq.features.push(
    ...rail_rq[0].stage1.features
  );
  await gridAndRailNetworks.railStages_rq.features.push(
    ...rail_rq[0].stage2.features
  );
  await gridAndRailNetworks.railStages_rq.features.push(
    ...rail_rq[0].stage3.features
  );
  await gridAndRailNetworks.railStages_rq.features.push(
    ...rail_rq[0].stage4.features
  );

  await gridAndRailNetworks.railStages_cq.features.push(
    ...rail_cq[0].stage1.features
  );
  await gridAndRailNetworks.railStages_cq.features.push(
    ...rail_cq[0].stage2.features
  );
  await gridAndRailNetworks.railStages_cq.features.push(
    ...rail_cq[0].stage3.features
  );
  await gridAndRailNetworks.railStages_cq.features.push(
    ...rail_cq[0].stage4.features
  );

  const bicycleGridBq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.gridStages_bq)
  );
  const bicycleGridRq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.gridStages_rq)
  );
  const bicycleGridCq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.gridStages_cq)
  );
  const bicycleRailBq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.railStages_bq)
  );
  const bicycleRailRq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.railStages_rq)
  );
  const bicycleRailCq = JSON.parse(
    JSON.stringify(gridAndRailNetworks.railStages_cq)
  );

  const cityData = JSON.parse(JSON.stringify(additionalCityData));
  const statsData = JSON.parse(JSON.stringify(statisticsData));

  return {
    bicycleGridBq,
    bicycleGridRq,
    bicycleGridCq,
    bicycleRailBq,
    bicycleRailRq,
    bicycleRailCq,
    cityData,
    statsData,
  };
}
