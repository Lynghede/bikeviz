import Head from "next/head";

import styled from "styled-components";
import Image from "next/image";
import Icon from "../Components/Icon";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState, useEffect, useRef } from "react";

import { getAllCitiesIds } from "../utilities/getImages";
import { cities } from "../public/CityData";

// import { Image as ImageIcon } from "@styled-icons/boxicons-regular";
// Icons
import { PlayBtnFill, ImageFill as ImageIcon } from "@styled-icons/bootstrap";
import { Download as DownloadSVG } from "@styled-icons/fa-solid/";
import { CloudDownload, FilePdf } from "@styled-icons/boxicons-solid";
import { CityDimensions } from "@styled-icons/fluentui-system-regular/City";

// Components
import { CityDownload } from "../Components/CityList";
import { ImageWrapper } from "../Components/Wrapper";
import { HeaderTop } from "../Components/Header/Header";
import {
  Container,
  Content,
} from "../Components/Documentation/Documentation.styled";
import {
  SearchCities,
  splitCityName,
  setCityLink,
} from "../Components/Location";

// Hooks
import { useOnClickOutside } from "../utilities/hooks";

const noImages = 8;
const noVideos = 18;

export async function getStaticProps() {
  const routes = await getAllCitiesIds();

  return {
    props: {
      routes: routes,
    },
  };
}

export default function Download({ ...props }) {
  const router = useRouter();
  return (
    <>
      <Head>
        <title> Download | GrowBike.Net</title>
        <link rel="icon" href="/android-icon-144x144.png" />
      </Head>
      <HeaderTop
        href="/download"
        icon={DownloadSVG}
        urltitle="Download"
        routeTitle=""
        sizeIcon="45"
      />
      <Content style={{ alignItems: "center" }}>
        <h1>Choose a city to access its download page </h1>
        <p>
          You can use the dropdown menu to either search, or select from a
          scrollable list
        </p>
        <p>
          Selecting a city from the list will take you to that city's download
          page
        </p>
      </Content>
      <DropDownMenuCities />
    </>
  );
}

export function DropDownMenuCities() {
  const router = useRouter();
  const [dropDownStatus, setDropDownStatus] = useState(false);
  const [filteredData, setFilteredData] = useState([]);
  // This makes sure the dropdown closes when clicking outside the dropdown menu or search field
  const node = useRef();
  useOnClickOutside(node, () => setDropDownStatus(false));
  const currentPath = router.asPath;

  // This useEffect makes sure that the dropdown closes when clicking on a city
  useEffect(() => {
    setDropDownStatus(false);
  }, [currentPath]);

  useEffect(() => {
    setFilteredData(cities);
  }, []);

  function isActive(selected) {
    const toLower = selected.toLowerCase();
    return toLower === router.query.id;
  }

  // Used to open the dropdown when clicking the search field
  const handleClickDropDownActive = () => {
    setDropDownStatus(true);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    result = Object.values(cities).filter((data) => {
      let citySearch = data.city.toLowerCase();
      let countrySearch = data.country.toLowerCase();
      let combined = citySearch + countrySearch;
      return combined.search(value) != -1;
    });

    setFilteredData(result);
  };

  return (
    <Container flexDir="column">
      <Content style={{ alignItems: "center" }}>
        <DropDownWrapper ref={node}>
          <SearchCities
            type="text"
            placeholder="Search or select city"
            required
            onFocus={handleClickDropDownActive}
            onChange={(event) => handleSearch(event)}
          ></SearchCities>
          {dropDownStatus ? (
            <DropDownSelectionWrapper>
              {Object.values(filteredData).map((value, index) => {
                const cityLowerCase = value.city.toLowerCase();
                return (
                  <Link
                    href={`/download/${setCityLink(cityLowerCase)}`}
                    key={index}
                  >
                    <CityDownload
                      active={isActive(value.city)}
                      value={value.city}
                      as="a"
                    >
                      <div>
                        <span style={{ fontSize: "18px", fontWeight: "bold" }}>
                          {splitCityName(value.city)}
                        </span>{" "}
                        <span style={{ fontSize: "14px" }}>
                          {value.country}
                        </span>
                      </div>
                    </CityDownload>
                  </Link>
                );
              })}
            </DropDownSelectionWrapper>
          ) : (
            <></>
          )}
        </DropDownWrapper>
      </Content>
    </Container>
  );
}

export const Overlay = (props) => {
  const hrefImage = props.hrefImage;

  return (
    <OverlayWrapper>
      <ClickableIconWrapper>
        <CustomATag href={hrefImage} download>
          <Icon icon={CloudDownload} sizeIcon="60" margin="0px" />
        </CustomATag>
        <CustomATag target="_blank" href={hrefImage}>
          <Icon icon={ImageIcon} sizeIcon="45" margin="0px" />
        </CustomATag>
      </ClickableIconWrapper>
    </OverlayWrapper>
  );
};

export const CardBoxImage = (props) => {
  const imageSrc = props.imageSrc ? props.imageSrc : "/_/_/_png";
  console.log(imageSrc);

  // Splits the src string, to reseemble a name for display instead.
  // Talk with Michael about what he want displayed.
  const search = "_";
  const replace = " ";
  const imageName = imageSrc
    .split("/")[1]
    .split("png")[0]
    .split(search)
    .join(replace);

  return (
    <CardWrapper>
      <Overlay hrefImage={`/Images/${imageSrc}`}></Overlay>

      <ImageWrapper>
        <Image src={`/Images/${imageSrc}`} layout="fill" objectFit="cover" />
      </ImageWrapper>
      <div style={{ display: "flex", marginTop: "5px" }}>
        <div>
          <Icon icon={ImageIcon} sizeIcon="40" margin="5px" />
        </div>
        <div>
          <CardHeader2>{imageName}</CardHeader2>
          <CardParagraph>Image</CardParagraph>
        </div>
      </div>
    </CardWrapper>
  );
};

export const CardBoxPDF = (props) => {
  const imageSrc = props.imageSrc ? props.imageSrc : "/_/_/pdf";
  console.log(imageSrc);

  // Splits the src string, to reseemble a name for display instead.
  // Talk with Michael about what he want displayed.
  const search = "_";
  const replace = " ";
  const pdfName = imageSrc
    .split("/")[1]
    .split("pdf")[0]
    .split(search)
    .join(replace);

  return (
    <CardWrapperPdf>
      <div style={{ display: "flex", marginTop: "5px" }}>
        <CustomATag href={`/Images/${imageSrc}`} download>
          <div>
            <Icon icon={FilePdf} sizeIcon="40" margin="5px" />
          </div>
        </CustomATag>
        <div>
          <CustomATag href={`/Images/${imageSrc}`} download>
            <CardHeader2>{pdfName}</CardHeader2>
            <CardParagraph>Pdf</CardParagraph>
          </CustomATag>
          <CustomATag target="_blank" href={`/Images/${imageSrc}`}>
            <CardParagraph>Preview</CardParagraph>
          </CustomATag>
        </div>
      </div>
    </CardWrapperPdf>
  );
};

export const CardBoxVideo = (props) => {
  const videoUrl = props.videoUrl;

  // Splits the src string, to reseemble a name for display instead.
  // Talk with Michael about what he want displayed.
  const search = "_";
  const replace = " ";
  const videoName = videoUrl
    .split("/")[1]
    .split("png")[0]
    .split(search)
    .join(replace);

  return (
    <CardWrapper>
      <ImageWrapper>
        <video
          controls
          width="100%"
          style={{
            background: "",
            maxHeight: "236px",
            display: "block",
          }}
        >
          <source src={`/Videos/${videoUrl}.webm`} type="video/mp4" />
        </video>
      </ImageWrapper>
      <div style={{ display: "flex", marginTop: "5px" }}>
        <div>
          <CustomATag href={`/Videos/${videoUrl}.mp4`} download>
            <Icon icon={CloudDownload} sizeIcon="40" margin="5px" />
          </CustomATag>
        </div>
        <div>
          <CardHeader2>{videoName}</CardHeader2>
          <CardParagraph>Video</CardParagraph>
        </div>
      </div>
    </CardWrapper>
  );
};

export const CardBoxWrapper = styled.div.attrs({
  className: "card_box_wrapper",
})`
  display: flex;
  border: ${(p) => p.theme.debugBorder} green solid;
  flex-wrap: wrap;
  justify-content: space-evenly;
  /* align-content: space-between; */
  width: 100%;
  margin-top: 10px;
`;

const CardWrapper = styled.div.attrs({ className: "card_wrapper" })`
  display: flex;
  flex-direction: column;
  //   position: relative;
  height: 300px;
  width: 344px;
  margin: 10px;
  border: ${(p) => p.theme.debugBorder} red solid;
  /* flex: 1 1 auto; */
`;

const CardWrapperPdf = styled.div.attrs({ className: "card_wrapper_pdf" })`
  display: flex;
  flex-direction: column;
  //   position: relative;
  height: 100px;
  width: 344px;
  margin: 10px;
  border: ${(p) => p.theme.debugBorder} red solid;
  /* flex: 1 1 auto; */
`;

const CardParagraph = styled.p`
  margin: 0;
  opacity: 70%;
  font-size: 0.8rem;
`;

const CardHeader2 = styled.h2`
  margin: 0;
  margin-top: 0;
  font-size: 1rem;
`;

const OverlayWrapper = styled.div.attrs({ className: "OverlayWrapper" })`
  display: flex;
  position: absolute;
  width: 344px;
  background: transparent;
  opacity: 0%;
  z-index: 20;
  min-height: 240px;
  justify-content: center;
  align-items: center;

  &:hover {
    opacity: 100%;
  }
`;

const ClickableIconWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-evenly;
  align-items: center;
  color: ${(p) => p.theme.body};
`;

const CustomATag = styled.a`
  &:hover {
    color: ${(p) => p.theme.highlight};
  }
`;

const DropDownSelectionWrapper = styled.div.attrs({
  className: "DropDownSelectionWrapper",
})`
  display: flex;
  overflow: auto;
  flex-direction: column;
  max-height: 20rem;
  scrollbar-width: none;
  position: absolute;
  background: ${(p) => p.theme.body};
  z-index: 100;
  top: 2rem;
  width: 100%;
  border: 2px solid white;
  border-radius: 3%;
`;

const DropDownWrapper = styled.div.attrs({
  className: "DropDownWrapper",
})`
  display: flex;
  flex-direction: column;
  width: 20rem;
  position: relative;
`;
