import { ThemeProvider } from "styled-components";
import { lightTheme, darkTheme } from "../theme";
import React, { useState } from "react";
import { GlobalStyles } from "../styles/global";
import { useDarkMode } from "../utilities/hooks";
import Head from "next/head";

function MyApp({ Component, pageProps }) {
  const [theme, toggleTheme] = useDarkMode();
  const themeMode = theme === "light" ? lightTheme : darkTheme;

  return (
    <div>
      <Head>
        <link
          href="https://api.mapbox.com/mapbox-gl-js/v2.2.0/mapbox-gl.css"
          rel="stylesheet"
        />
      </Head>
      <ThemeProvider theme={themeMode}>
        <GlobalStyles />
        <Component theme={theme} toggleTheme={toggleTheme} {...pageProps} />
      </ThemeProvider>
    </div>
  );
}

export default MyApp;
