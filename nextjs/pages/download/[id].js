import { cities } from "../../public/CityData";
import { getImagesRoutes, getVideoRoutes } from "../../utilities/getImages";
import styled from "styled-components";

// Components
import Head from "next/head";
import { HeaderTop } from "../../Components/Header/Header";
import { Container } from "../../Components/Documentation/Documentation.styled";
import {
  CardBoxImage,
  CardBoxPDF,
  CardBoxVideo,
  CardBoxWrapper,
  DropDownMenuCities,
} from "../download";
import { H1 } from "../../Components/Documentation/Documentation.styled";
import { capitalID } from "../../Components/CityList";
import { ResetButton } from "../../Components/Filters";
import { splitCityName } from "../../Components/Location";

// Icons
import { Download as DownloadSVG } from "@styled-icons/fa-solid/";
import { CloudDownload } from "@styled-icons/boxicons-solid";
import { Cloud } from "@styled-icons/bootstrap";

export async function getStaticProps({ params }) {
  const paths = await params.id;
  const pictures = await getImagesRoutes(paths);
  const videos = await getVideoRoutes(paths);

  return {
    props: {
      pictures: pictures,
      videos: videos,
      paths: paths,
    },
  };
}

export async function getStaticPaths() {
  const ids = Object.keys(cities);
  return {
    paths: ids.map((id) => ({ params: { id } })),
    fallback: false,
  };
}

export default function DownloadPage({ pictures, paths, videos }) {
  // console.log({ pictures }); // returns list of pictures
  // console.log({ paths }); // returns the name of the city
  // console.log({ videos }); // returns list of videos

  const listOfVideos = [];
  const listOfImages = [];
  const listOfPdfs = [];

  for (let video of videos) {
    if (video.endsWith(".zip")) {
      // do nothing
    } else {
      listOfVideos.push(video.split(".")[0]);
    }
  }

  for (let image of pictures) {
    if (image.endsWith(".png")) {
      listOfImages.push(image);
    }
  }

  for (let pdf of pictures) {
    if (pdf.endsWith(".pdf")) {
      listOfPdfs.push(pdf);
    }
  }

  const uniqueListOfVideos = [...new Set(listOfVideos)];

  return (
    <>
      <>
        <Head>
          <title> Download | GrowBike.Net</title>
          <link rel="icon" href="/android-icon-144x144.png" />
        </Head>
        <HeaderTop
          href="/download"
          icon={DownloadSVG}
          urltitle="Download"
          routeTitle={capitalID(paths)}
          sizeIcon="45"
        />
        <DropDownMenuCities />
        <Container flexDir={"column"}>
          <div style={{ justifyContent: "center", display: "flex" }}>
            <ButtonWrapper>
              <div>
                <a href={`/Videos/${paths}/${paths}.zip`} download>
                  <ResetButton fontSize={"1rem"}>
                    Download all Videos{" "}
                    <span>
                      <CloudDownload size={30} />
                    </span>
                  </ResetButton>
                </a>
              </div>
              <div>
                <a href={`/Images/${paths}/${paths}.zip`} download>
                  <ResetButton fontSize={"1rem"}>
                    Download all Images{" "}
                    <span>
                      <CloudDownload size={30} />
                    </span>
                  </ResetButton>
                </a>
              </div>
            </ButtonWrapper>
          </div>
          {uniqueListOfVideos.length > 1 ? (
            <>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <H1>Videos</H1>
              </div>
              <CardBoxWrapper>
                {uniqueListOfVideos.map((video, index) => (
                  <CardBoxVideo key={index} videoUrl={`${paths}/${video}`} />
                ))}
              </CardBoxWrapper>
            </>
          ) : (
            <></>
          )}
          {listOfImages.length > 1 ? (
            <>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <H1>Images</H1>
              </div>
              <CardBoxWrapper>
                {listOfImages.map((route, index) => (
                  <CardBoxImage key={index} imageSrc={`${paths}/${route}`} />
                ))}
              </CardBoxWrapper>
            </>
          ) : (
            <></>
          )}
          {listOfPdfs.length > 1 ? (
            <>
              <div style={{ display: "flex", justifyContent: "center" }}>
                <H1>PDF Images</H1>
              </div>
              <CardBoxWrapper>
                {listOfPdfs.map((route, index) => (
                  <CardBoxPDF key={index} imageSrc={`${paths}/${route}`} />
                ))}
              </CardBoxWrapper>
            </>
          ) : (
            <></>
          )}
        </Container>
      </>
    </>
  );
}

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  width: 50%;
`;
