import Head from "next/head";
import React, { useRef, useState, useEffect } from "react";
import { useRouter } from "next/router";
import { cities } from "../../public/CityData";
import LoadingScreen from "../../Components/LoadingScreen";
import { getAllStages } from "../../database/mongodb";
// functions
import {
  addSourcesAndLayers,
  addPathLayer,
  addExistingLayer,
  addGridPOI,
} from "../../map/mapFunctions";
import { measureTime } from "../../utilities/functions";

// Components
import Slider from "../../Components/Slider";
import Sidebar from "../../Components/Sidebar";
import NavigationBar from "../../Components/NavigationBar";

// Mapbox GL JS
const mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");

// Can cause security breach if public like this. Should be removed before future website is made.

mapboxgl.accessToken =
  "pk.eyJ1IjoibHluZ2hlZGUiLCJhIjoiY2tmbzF2NTNyMDN1dzJycGFoY2RrN2Z1eiJ9.1res_x4cop2qXU1ZpyJ_Tw";

export default function City({ initialCityData, ...props }) {
  const router = useRouter();
  const cityData = initialCityData;

  const bicycleRailBq = initialCityData.bicycleRailBq;
  const bicycleRailCq = initialCityData.bicycleRailCq;
  const bicycleRailRq = initialCityData.bicycleRailRq;
  const bicycleGridBq = initialCityData.bicycleGridBq;
  const bicycleGridCq = initialCityData.bicycleGridCq;
  const bicycleGridRq = initialCityData.bicycleGridRq;
  const statsRailBq = initialCityData.statsData[0].statsRailBq;
  const statsRailCq = initialCityData.statsData[0].statsRailCq;
  const statsRailRq = initialCityData.statsData[0].statsRailRq;
  const statsGridBq = initialCityData.statsData[0].statsGridBq;
  const statsGridCq = initialCityData.statsData[0].statsGridCq;
  const statsGridRq = initialCityData.statsData[0].statsGridRq;
  // used for non existing cityData in the database
  const placeholderData = [
    {
      _id: "",
      id: router.query.id,
      pointer: { x: "" },
      statsBicycleGrid: { y: "" },
      statsBicycleRailway: { z: "" },
    },
  ];
  const bicycleInfo =
    initialCityData.cityData.length < 1
      ? placeholderData
      : initialCityData.cityData;

  const mapContainerRef = useRef(null);
  const mapInstanceRef = useRef(null);
  const [slider, setSlider] = useState(0);
  const [isMapboxLoaded, setIsMapboxLoaded] = useState(false);
  const city = bicycleInfo[0].id;
  const existing = bicycleInfo[0].existingBicycle;
  const poiRail = bicycleInfo[0].poiRail;
  const poiGrid = bicycleInfo[0].poiGrid;
  const [showExisting, setShowExisting] = useState(false);
  const [showSlider, setShowSlider] = useState(true);
  const [poi, setPoi] = useState("grid");
  const [growthStrategy, setGrowthStrategy] = useState("bq");
  const [statsDisplayed, setStatsDisplayed] = useState(statsRailBq);
  const [poiDisplayed, setPoiDisplayed] = useState(poiRail);
  const [option, setOption] = useState("Location");
  const statsCity = cities[router.query.id].city;
  let layerDisplayed = bicycleGridBq;

  function setNetworkData() {
    if (poi === "rail") {
      setPoiDisplayed(poiRail);

      if (growthStrategy === "bq") {
        layerDisplayed = bicycleRailBq;
        setStatsDisplayed(statsRailBq);
      }
      if (growthStrategy === "cq") {
        layerDisplayed = bicycleRailCq;
        setStatsDisplayed(statsRailCq);
      }
      if (growthStrategy === "rq") {
        layerDisplayed = bicycleRailRq;
        setStatsDisplayed(statsRailRq);
      }
    }
    if (poi === "grid") {
      setPoiDisplayed(poiGrid);

      if (growthStrategy === "bq") {
        layerDisplayed = bicycleGridBq;
        setStatsDisplayed(statsGridBq);
      }
      if (growthStrategy === "cq") {
        layerDisplayed = bicycleGridCq;
        setStatsDisplayed(statsGridCq);
      }
      if (growthStrategy === "rq") {
        layerDisplayed = bicycleGridRq;
        setStatsDisplayed(statsGridRq);
      }
    }
  }

  const emptyNetworkLayer = {
    type: "GeometryCollection",
    geometries: [],
  };

  const initialStyle =
    props.theme === "light"
      ? "mapbox://styles/mapbox/streets-v11"
      : "mapbox://styles/mapbox/dark-v10";
  // initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainerRef.current,
      style: initialStyle,
      center: [2.32, 48.854],
      zoom: 12,
    });

    map.on("load", () => {
      addSourcesAndLayers(map);
      addPathLayer(map);
      addExistingLayer(map);
      addGridPOI(map);
      setIsMapboxLoaded(true);
    });

    // "once" makes it run only on the first city you visit
    map.once("moveend", () => {
      setIsPlaying(true);
    });

    mapInstanceRef.current = map;
    // clean up on unmount
    return () => map.remove();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  // Setting the "points of interest" data
  useEffect(() => {
    if (!isMapboxLoaded) return;
    const source = mapInstanceRef.current.getSource("poi");
    const gridSource = mapInstanceRef.current.getSource("gridPoi");
    if (!source || !gridSource) return;

    setNetworkData();

    if (poi === "rail") {
      gridSource.setData(emptyNetworkLayer);
      source.setData(poiDisplayed);
    }
    if (poi === "grid") {
      source.setData(emptyNetworkLayer);
      gridSource.setData(poiDisplayed);
    }
  }, [isMapboxLoaded, bicycleInfo, poiDisplayed, poi]);

  useEffect(() => {
    mapChangeCenter(router.query.id);
  }, [router.query.id]);

  useEffect(() => {
    if (!isMapboxLoaded) return;

    const source = mapInstanceRef.current.getSource("bicyclepaths");
    if (!source) return;
    setShowSlider(true);
    setNetworkData();

    const sliderData = {
      type: "FeatureCollection",
      features: [],
    };

    if (!showSlider) {
      setShowSlider(false);
    }

    layerDisplayed.features
      .slice(0, slider + 1)
      .forEach((element) => sliderData.features.push(element[0]));
    source.setData(sliderData);
  }, [isMapboxLoaded, slider, layerDisplayed, poi, growthStrategy, showSlider]);

  //Add the existing bicycle network to the map, depending on the value of the checkbox
  useEffect(() => {
    if (!isMapboxLoaded) return;
    const source = mapInstanceRef.current.getSource("existingnetwork");
    if (!source) return;

    if (showExisting) {
      source.setData(existing);
    } else {
      source.setData(emptyNetworkLayer);
    }
  }, [isMapboxLoaded, showExisting, cityData]);

  function forEachLayer(text, cb) {
    mapInstanceRef.current.getStyle().layers.forEach((layer) => {
      if (!layer.id.includes(text)) return;

      cb(layer);
    });
  }

  const [isPlaying, setIsPlaying] = useState(true);
  function handlePlay(e) {
    e.preventDefault();
    if (isPlaying) {
      setIsPlaying(!isPlaying);
    }
    if (!isPlaying) {
      setIsPlaying(!isPlaying);
    }
  }

  useEffect(() => {
    if (!isMapboxLoaded) return;
    const source = mapInstanceRef.current.getSource("bicyclepaths");
    if (!source) return;

    setNetworkData();

    const sliderData = {
      type: "FeatureCollection",
      features: [],
    };

    if (isPlaying) {
      const interval = setInterval(() => {
        if (slider < layerDisplayed.features.length - 1) {
          setSlider(slider + 1);

          layerDisplayed.features
            .slice(0, slider + 1)
            .forEach((element) => sliderData.features.push(element[0]));
          source.setData(sliderData);
        } else {
          clearInterval(interval);
        }
      }, 1000);
      return () => clearInterval(interval);
    }
  }, [isMapboxLoaded, isPlaying, slider, layerDisplayed, poi, growthStrategy]);

  // Changing the map base style
  function changeStyle(style) {
    const savedLayers = [];
    const savedSources = {};
    const layerGroups = ["paths", "pointOfInterest"];

    // Tracks the status of isPlaying
    const changeStyleIsPlaying = isPlaying;
    setIsPlaying(false);

    layerGroups.forEach((layerGroup) => {
      forEachLayer(layerGroup, (layer) => {
        savedSources[layer.source] = mapInstanceRef.current
          .getSource(layer.source)
          .serialize();
        savedLayers.push(layer);
      });
    });

    mapInstanceRef.current.setStyle(`mapbox://styles/mapbox/${style}`);

    setTimeout(() => {
      Object.entries(savedSources).forEach(([id, source]) => {
        mapInstanceRef.current.addSource(id, source);
      });

      savedLayers.forEach((layer) => {
        mapInstanceRef.current.addLayer(layer);
      });
      setIsPlaying(changeStyleIsPlaying);
    }, 1000);
  }

  function switchLayer() {
    if (props.theme === "dark") {
      changeStyle("streets-v11", mapInstanceRef);
    }
    if (props.theme === "light") {
      changeStyle("dark-v10", mapInstanceRef);
    }
  }

  function handleChangeStyle(style) {
    changeStyle(style, mapInstanceRef);
  }

  function mapChangeCenter(id) {
    setIsPlaying(false);
    setSlider(0);
    setShowExisting(false);

    mapInstanceRef.current.flyTo({
      center: cities[id].coordinates,
      zoom: cities[id].zoom,
    });
  }

  const tabDisplaycity = city[0].toUpperCase() + city.slice(1);

  return (
    <>
      <Head>
        <title> {tabDisplaycity} | GrowBike.Net</title>
      </Head>
      <div>
        <main>
          <div className="map-container" ref={mapContainerRef} />

          {!isMapboxLoaded ? (
            <LoadingScreen />
          ) : (
            <div>
              <Sidebar
                switchLayer={switchLayer}
                changeStyle={handleChangeStyle}
                themeProps={props}
                showExisting={showExisting}
                setShowExisting={setShowExisting}
                showSlider={showSlider}
                isPlaying={isPlaying}
                setShowSlider={setShowSlider}
                setIsPlaying={setIsPlaying}
                option={option}
                setOption={setOption}
              ></Sidebar>

              <Slider
                min="0"
                max="40"
                step="1"
                value={slider}
                onChange={setSlider}
                handlePlay={handlePlay}
                isPlaying={isPlaying}
                setIsPlaying={setIsPlaying}
                showSlider={showSlider}
                stats={statsDisplayed}
                poi={poi}
                setPoi={setPoi}
                growthStrategy={growthStrategy}
                setGrowthStrategy={setGrowthStrategy}
              />

              <NavigationBar
                stats={statsDisplayed}
                city={city}
                statsCity={statsCity}
                themeProps={props}
                slider={slider}
                optionLayer={option}
                setOptionLayer={setOption}
              />
            </div>
          )}
        </main>
      </div>
    </>
  );
}

export async function getStaticProps({ params }) {
  const city = await getAllStages(params.id);
  return {
    props: {
      initialCityData: JSON.parse(JSON.stringify(city)),
    },
  };
}

export async function getStaticPaths() {
  const ids = Object.keys(cities);
  return {
    paths: ids.map((id) => ({ params: { id } })),
    fallback: false,
  };
}
