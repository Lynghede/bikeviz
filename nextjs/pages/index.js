import Head from "next/head";

import React, { useEffect } from "react";
// const mapboxgl = require("mapbox-gl/dist/mapbox-gl.js");
import { useRouter } from "next/router";
import { cities } from "../public/CityData";

export default function Home() {
  const router = useRouter();
  //redirects the user to the closest city with data
  function findClosestsCoords(coordLat, coordLng) {
    let locArray = [];
    Object.values(cities).forEach((element) => {
      locArray.push(element);
    });
    let distance = calculateDistance(
      coordLat,
      coordLng,
      locArray[0].coordinates[1],
      locArray[0].coordinates[0]
    );
    let newCity = locArray[0].city;
    for (let city of locArray) {
      let newDistance = calculateDistance(
        coordLat,
        coordLng,
        city.coordinates[1],
        city.coordinates[0]
      );
      if (newDistance < distance) {
        distance = newDistance;
        newCity = city.city;
      }
    }
    router.push("/city/" + newCity.toLowerCase());
  }

  function calculateDistance(ogLat, ogLng, cityLat, cityLng) {
    return Math.sqrt(
      Math.pow(ogLat - cityLat, 2) + Math.pow(ogLng - cityLng, 2)
    );
  }

  useEffect(() => {
    async function getPosition() {
      try {
        if (
          location.protocol !== "https:" &&
          location.hostname !== "localhost"
        ) {
          throw new Error("No access to geolation API");
        }

        const position = await new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition(resolve, reject);
        });

        var lng = position.coords.longitude;
        var lat = position.coords.latitude;
        findClosestsCoords(lat, lng);
      } catch (e) {
        router.push("/city/paris");
      }
    }
    getPosition();
  }, []);

  return (
    <div>
      <Head>
        <title>GrowBike.Net</title>
        <link rel="icon" href="/android-icon-144x144.png" />
      </Head>
    </div>
  );
}
