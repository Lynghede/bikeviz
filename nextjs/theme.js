export const lightTheme = {
  body: "#E2E2E2",
  text: "#363537",
  toggleBorder: "#FFF",
  gradient: "linear-gradient(#39598A, #79D7ED)",
  debugBorder: "0px",
  primaryDiv: "#EEF1F4",
  secondaryDiv: "rgb(230, 232, 235)",
  secondaryDivOpacity: "rgb(230, 232, 235, 0.7)",
  grey: "#bebebe",
  background: "#ffffff",
  outline: "black",
  highlight: "#0C5FE4",
};

export const darkTheme = {
  body: "#363537",
  text: "#FAFAFA",
  toggleBorder: "#6B8096",
  gradient: "linear-gradient(#091236, #1E215D)",
  debugBorder: "0px",
  primaryDiv: "#29323C",
  secondaryDiv: "rgb(36, 39, 48)",
  secondaryDivOpacity: "rgb(36, 39, 48, 0.7)",
  grey: "#828282",
  background: "#353a47",
  outline: "#FAFAFA",
  highlight: "#4C8DF6",
};
