import styled, { keyframes } from "styled-components";
import { MdDirectionsBike } from "react-icons/md";

const LoadingScreen = () => {
  return (
    <>
      <LoadingScreenWrapper>
      <TextContainer> <MdDirectionsBike size={50} /> <Text> | GrowBike.Net </Text></TextContainer>
      <IconWrapper>
        <Loading>Loading</Loading>
        </IconWrapper>
      </LoadingScreenWrapper>
    </>
  );
};

export default LoadingScreen;

const LoadingScreenWrapper = styled.div.attrs({
  className: "LoadingScreenWrapper",
})`
  height: 100%;
  width: 100%;
  position: absolute;
  background-color: #353a47;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${(p) => p.theme.text};
`;

const IconWrapper = styled.div.attrs({
  className: "IconWrapper",
})`
  display: flex;
  justify-content: flex-start;
  margin-left: 10px;
  position: relative;
  background: transparent;
  color: #ffffff;
  height: 30px;
`;

const Text = styled.div`
color: white;
font-size: 40px;
margin-left: 10px;
`;

const TextContainer = styled.div.attrs({
  className: "TextContainer",
})`
  display: flex;
  flex-direction: row;
  align-self: center;
  justify-content: center;
  top: 50px;
  text-align: center;
  position: absolute;
  color: white;
`;


const dots = keyframes`
0%   { content: ''; }
25%  { content: '.'; }
50%  { content: '..'; }
75%  { content: '...'; }
100% { content: ''; }
`;

const Loading = styled.div`
  color: white;
  font-size: 35px;
  margin-left: 10px;

  :after {
    display: inline-block;
    animation: ${dots} steps(1, end) 1s infinite;
    content: "";
  }
`;
