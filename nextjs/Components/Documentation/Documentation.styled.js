import styled from "styled-components";

export const A = styled.a.attrs({ className: "Styled_A" })`
  color: ${(p) => (p.active ? p.theme.highlight : "")};
  font-weight: ${(props) => (props.active ? "bold" : "normal")};
  cursor: pointer;
  :hover {
    color: ${(p) => p.theme.highlight};
  }
`;

export const Page = styled.div.attrs({ className: "Page" })`
  border: ${(p) => p.theme.debugBorder} solid red;
  display: flex;
  flex-direction: column;
  background: ${(p) => p.theme.secondaryDiv};
  width: 100%;
  height: 100%;
`;

export const Header = styled.header.attrs({ className: "Header" })`
  border: ${(p) => p.theme.debugBorder} solid black;
  display: flex;
  flex-direction: row;
  height: 80px;
  width: 100%;
  background: ${(p) => p.theme.primaryDiv};
  justify-content: space-evenly;
  align-items: center;
  position: relative;
  z-index: 10;
  box-shadow: 0 3px 8px 0 rgba(116, 129, 141, 0.1);
  border-bottom: 1px solid #d4dadf;
`;

export const Wrapper = styled.div.attrs({ className: "Wrapper" })`
  border: ${(p) => p.theme.debugBorder} solid blue;
  display: flex;
  align-items: center;
  width: max-content;
  h1 {
    font-size: 23px;
  }
  span {
    color: ${(p) => p.theme.highlight};
  }
  :hover {
    cursor: pointer;
  }
`;

export const NavigationMenu = styled.nav.attrs({ className: "Navigationmenu" })`
  border-right: 1px solid ${(p) => p.theme.text};
  box-shadow: 0 0px 8px 5px rgba(116, 129, 141, 0.1);
  min-width: 250px;
  flex-direction: column;
  padding-left: 10px;
  display: block;
  position: sticky;
  height: 100vh;
  top: 0px;
`;

export const Container = styled.div.attrs({ className: "Container" })`
  border: ${(p) => p.theme.debugBorder} solid yellow;
  display: flex;
  flex-direction: ${(p) => (p.flexDir ? p.flexDir : "row")};
  width: 80%;
  height: 100%;
  margin: 0 auto;
`;

export const Content = styled.div.attrs({ className: "Content" })`
  display: flex;
  width: auto;
  border: ${(p) => p.theme.debugBorder} solid green;
  flex-direction: column;
  width: 100%;
  padding: 15px;
`;

export const H1 = styled.h1`
  /* color: ${(p) => p.theme.highlight}; */
  font-size: 2rem;
`;

export const H2 = styled.h2`
  /* color: ${(p) => p.theme.highlight}; */
`;
