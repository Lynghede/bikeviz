import React from "react";
import { NetworkChart, Play, Pause } from "@styled-icons/boxicons-regular";
import { RestartAlt } from "@styled-icons/material/";
import styled from "styled-components";
import {SyntheticNetworks} from "./SyntheticNetworks";


const Slider = ({ onChange, ...props }) => {
  function handleChange(e) {
    onChange(parseInt(e.target.value), 10);
    props.setIsPlaying(false);
  }

  function handleRestart(e) {
    e.preventDefault();
    onChange(0);
    props.setIsPlaying(true);
  }
  const statsValue = props.stats.length > 0 ? props.stats[props.value-1] : 0;

  return (
    <SliderWrapper>
      <RowWrapper>
        <PlayButtonWrapper showSlider={props.showSlider}>
          <PlayButton
            onClick={props.value === 40 ? handleRestart : props.handlePlay}
          >
            {props.value === 40 ? (
              <RestartAlt size="40" />
            ) : props.isPlaying ? (
              <Pause size="40" />
            ) : (
              <Play size="40" />
            )}
          </PlayButton>
        </PlayButtonWrapper>
        <StyledSliderInput showSlider={props.showSlider} type="range" onChange={handleChange} {...props} />
      </RowWrapper>
      <RowWrapper>
          <SyntheticNetworks
          poi={props.poi}
          setPoi={props.setPoi}
          growthStrategy={props.growthStrategy}
          setGrowthStrategy={props.setGrowthStrategy}
          />
      <StageSliderWrapper showSlider={props.showSlider}>
        <NetworkChart size="50" />
        <div style={{ marginLeft: "0.5rem" }}>
          <p>
            Stage <ColoredStageNumber>{props.value}</ColoredStageNumber>{" "}
            <span>| {props.value === 0 ? "0" : (statsValue.length / 1000).toFixed(0)}  km </span>
          </p>
        </div>
      </StageSliderWrapper>
      </RowWrapper>
    </SliderWrapper>
  );
};

export default Slider;

const ColoredStageNumber = styled.span`
  color: black;
  font-weight: bold;
`;

const StageSliderWrapper = styled.div.attrs({ className: "Stages_wrapper" })`
  display: ${(props) => (props.showSlider ? "flex" : "none")};  
  background: ${(p) => p.theme.grey};
  border: 1px ${(p) => p.theme.grey} solid;
  border-radius: 50px;
  padding: 10px;
  align-items: center;
  margin: 1rem 0 1rem 0;
  p {
    font-size: 1.5em;
    margin: 0;
  }
`;

const PlayButton = styled.button.attrs({ className: "PlayButton" })`
  display: flex;
  height: 50px;
  width: 50px;
  border: 2px solid ${(p) => p.theme.text};
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  background: ${(p) => p.theme.secondaryDiv};
  color: ${(p) => p.theme.text};

  :hover {
    background: ${(p) => p.theme.text};
    color: ${(p) => p.theme.secondaryDiv};
  }
`;

const RowWrapper = styled.div.attrs({ className: "Row_Wrapper" })`
  display: flex;
  flex-direction: row;
  border: ${(p) => p.theme.debugBorder} solid blue;
  align-items: center;
  width: 100%;

  @media screen and (max-width: 800px) {
    flex-direction: column-reverse;
  }
`;

const PlayButtonWrapper = styled.div.attrs({
  className: "Play_Button_Wrapper",
})`
  display: ${(props) => (props.showSlider ? "flex" : "none")};
  border: ${(p) => p.theme.debugBorder} solid green;
  position: absolute;
  left: -60px;

  @media screen and (max-width: 800px) {
    position: initial;
    margin-top: 5px;
  }
`;

const SliderWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  bottom: 0;
  left: 0;
  right: 0;
  position: absolute;
  width: 45%;
  align-items: center;
  border: ${(p) => p.theme.debugBorder} solid red;
`;

const StyledSliderInput = styled.input`
  -webkit-appearance: none;
  display: ${(props) => (props.showSlider ? "flex" : "none")};
  background: aliceblue;
  border-radius: 10px;
  border: 1px ${(p) => p.theme.grey} solid;
  outline: none;
  width: 100%;
  margin: 0;
  position: relative;
  cursor: ew-resize;
  height: 10px;
  opacity: 0.7;
  -webkit-transition: 0.2s;
  transition: opacity 0.2s;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    background: ${(p) => p.theme.grey};
    cursor: pointer;
  }

  &::-moz-range-thumb {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background: ${(p) => p.theme.grey};
    cursor: pointer;
  }

  &:hover {
    opacity: 1;
  }
`;
