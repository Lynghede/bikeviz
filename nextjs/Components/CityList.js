import styled from "styled-components";

export const CityDownload = styled.div.attrs({ className: "CityDownload" })`
  display: flex;
  padding: 5px;
  /* min-width: fit-content; */
  flex-direction: column;
  background-color: ${(props) =>
    props.active ? props.theme.grey : props.theme.background};
  margin-bottom: ${(props) =>
    props.marginBottom ? props.marginBottom : "0px"};
  color: ${(p) => p.theme.text};
  border: ${(p) => (p.border ? "2px solid " + p.theme.outline : "")};
  border-radius: 3px;
  text-align: left;
  text-transform: uppercase;
  width: 100%;
  max-width: ${(p) => (p.maxWidth ? p.maxWidth : "")};

  :hover {
    background: ${(p) => p.theme.text};
    color: ${(p) => p.theme.background};
  }
`;

export const City = styled.div.attrs({ className: "City" })`
  display: flex;
  padding: 5px;
  flex-direction: column;
  margin-bottom: ${(props) =>
    props.marginBottom ? props.marginBottom : "0px"};
  border: ${(p) => (p.border ? "2px solid " + p.theme.outline : "")};
  border-radius: 3px;
  text-align: left;
  text-transform: uppercase;
  width: 100%;
  max-width: ${(p) => (p.maxWidth ? p.maxWidth : "")};
`;

export const capitalID = (route) => {
  let ID = "";
  if (route === "losangeles") {
    ID = "Los Angeles";
  } else if (route === "telaviv") {
    ID = "Tel Aviv";
  } else if (route === "saopaulo") {
    ID = "Sao Paulo";
  } else if (route === "sanfrancisco") {
    ID = "San Francisco";
  } else if (route === "mexico") {
    ID = "Mexico City";
  } else if (route === "shahalam") {
    ID = "Shah Alam";
  } else if (route === "santiago") {
    ID = "Santiago Centro";
  } else if (route === "manchestergreater") {
    ID = "Manchester";
  } else if (route === "buenosaires") {
    ID = "Buenos Aires";
  } else if (route === "hongkong") {
    ID = "Hong Kong";
  } else {
    ID = route[0].toUpperCase() + route.slice(1);
  }
  return ID;
};
