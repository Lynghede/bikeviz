import styled from "styled-components";

const Icon = (props) => {
  const DynamicIcon = props.icon;
  const margin = props.margin;
  const sizeIcon = props.sizeIcon;
  const StyledIcon = styled(DynamicIcon)`
    margin-right: ${margin};
  `;

  return <StyledIcon size={sizeIcon} />;
};

export default Icon;
