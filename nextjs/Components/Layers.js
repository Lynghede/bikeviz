import styled from "styled-components";
import { IoInformationCircleOutline } from "react-icons/io5";
import Tooltip from "@material-ui/core/Tooltip";
import { OptionWrapper } from "./Location";
import {
  ResetButton,
  CheckBoxWrapper,
  CheckBoxLabel,
  CheckBox,
  TitleHeader,
} from "./Filters";
import { useState } from "react";

const Layers = (props) => {
  const statusPlay = props.isPlaying;

  const handleCheckedChange = (e) => {
    const checkedValue = e.target.checked;
    if (e.target.value === "slider") {
      props.setShowSlider(checkedValue);
      if (statusPlay) {
        props.setIsPlaying(checkedValue);
      }
    }
  };

  const handleCheckedExisting = (e) => {
    const checkedValue = e.target.checked;
    if (e.target.value === "existing") {
      props.setShowExisting(checkedValue);
    }
  }
  
  function handleReset() {
    props.setShowExisting(false);
    props.setShowSlider(true);
  }

  return (
    <OptionWrapper>
      <TitleHeader> Layers </TitleHeader>
      <div>
        <CheckBoxWrapper>
          <CheckBox
            id="checkboxexisting"
            type="checkbox"
            value="existing"
            onChange={handleCheckedExisting}
            checked={props.showExisting}
          />
          <CheckBoxLabel htmlFor="checkboxexisting" />
          <p>Existing Network</p>
          <Tooltip
            title={"Displays the existing bicycle network"}
            placement="left"
            arrow
          >
            <IconWrapper>
              <IoInformationCircleOutline size={15} />
            </IconWrapper>
          </Tooltip>
        </CheckBoxWrapper>
      </div>
      <div>
        <CheckBoxWrapper>
          <CheckBox
            id="checkboxslider"
            type="checkbox"
            value="slider"
            //disabled={disableSlider}
            onChange={handleCheckedChange}
            checked={props.showSlider}
          />
          <CheckBoxLabel htmlFor="checkboxexisting" />
          <p> Slider </p>
          <Tooltip title={"Displays the slider"} placement="left" arrow>
            <IconWrapper>
              <IoInformationCircleOutline size={15} />
            </IconWrapper>
          </Tooltip>
        </CheckBoxWrapper>
      </div>
      <ResetButton onClick={handleReset}>Reset Layers</ResetButton>
    </OptionWrapper>
  );
};

export default Layers;

const IconWrapper = styled.div.attrs({ className: "IconWrapper" })`
  display: flex;
  margin-left: 5px;
  margin-top: 3px;
  background: transparent;
`;
