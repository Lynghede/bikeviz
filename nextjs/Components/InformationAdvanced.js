import styled from "styled-components";
import { IoMdClose } from "react-icons/io";
import { IoLogoBuffer } from "react-icons/io5";
import { WindowContainer, WindowWrapper } from "./Statistics";
import Image from "next/image";
import { useState } from "react";

const StyledLayerIcon = styled(IoLogoBuffer)`
  :hover {
    color: #0eb6d2;
  }
`;

const InformationAdvanced = (props) => {
  const [showImage, setShowImage] = useState(false);

  function handleSeeFigureClick() {
    setShowImage(!showImage);
  }

  function handleLayerIconClick() {
    props.setOptionLayer("Layer");
  }

  return (
    <>
      <WindowWrapper
        style={{ minWidth: "750px", maxHeight: "575px", overflow: "auto" }}
      >
        <IoMdClose
          size={20}
          style={{ position: "absolute", top: 5, left: 5 }}
          onClick={props.clickCross}
        ></IoMdClose>
        <WindowContainer>
          <div
            style={{
              fontSize: "28px",
              marginBottom: "5px",
              fontWeight: "bold",
            }}
          >
            GrowBike.Net
          </div>
        </WindowContainer>
        <SectionTitle>Grow bicycle networks in your city</SectionTitle>
        <InformationMessage>
          Explore how to grow bicycle networks from scratch. <br /> <br />
          Choose a city and grow the bike network, connecting places efficiently
          step by step.
        </InformationMessage>
        <SectionTitle>Connecting points of interest </SectionTitle>
        {showImage ? (
          <ImageWrapper>
            <Image
              src="/fig3.png"
              alt="Network Figures"
              layout="fill"
              loading="eager"
              objectFit="contain"
            />
          </ImageWrapper>
        ) : (
          <div />
        )}

        <InformationMessage>
          Our growth algorithms follow 4 steps{" "}
          <SeeFigure onClick={handleSeeFigureClick}> see figure</SeeFigure>:
          <ol>
            <li>
              Select points of interests:{" "}
              <NetworkButton style={{ width: "40px" }}>Rail</NetworkButton>{" "}
              stations, or{" "}
              <NetworkButton style={{ width: "40px" }}>Grid</NetworkButton>{" "}
              points
            </li>{" "}
            <br />
            <li>
              Create a triangulation between them: Connect them stepwise without
              link crossings
            </li>{" "}
            <br />
            <li>
              Order the links by a growth strategy:{" "}
              <NetworkButton style={{ width: "30px" }}>B</NetworkButton>{" "}
              etweenness,{" "}
              <NetworkButton style={{ width: "30px" }}>C</NetworkButton>{" "}
              loseness, or{" "}
              <NetworkButton style={{ width: "30px" }}>R</NetworkButton> andom
            </li>{" "}
            <br />
            <li>Route the links on the city's street network</li> <br />
          </ol>
          This process creates a cohesive bicycle network inspired by the CROW design manual for bicycle traffic - something that every modern city should have.
          Studying these synthetic networks informs us about the geometric
          limitations of urban bicycle network growth and can lead to better
          designed bicycle infrastructure in cities. Compare the grown networks
          with your city's existing bicycle network at{" "}
          <StyledLayerIcon
            onClick={handleLayerIconClick}
            size={12}
          ></StyledLayerIcon>
          . <br /> <br /> As of now our synthetic networks should not be taken
          as concrete recommendations on where to build new bicycle
          infrastructure but as initial visions of cohesive bicycle networks to
          be refined subsequently.
        </InformationMessage>
        <SectionTitle>Download media</SectionTitle>
        <InformationMessage>
          Find over 1000 videos and plots on our downloads page.{" "}
          <LinkStyle href={"http://www.growbike.net/download"}>
            http://www.growbike.net/download
          </LinkStyle>
          <br /> <br />
          The material on this web site can be used freely in any publication
          provided that:
          <ol>
            <li>
              It is duly credited as a project by NEtwoRks, Data, and Society
              (NERDS), ITU Copenhagen
            </li>{" "}
            <br />
            <li>A PDF copy of the publication is sent to misz@itu.dk</li>
          </ol>
        </InformationMessage>
        <SectionTitle>Read the paper</SectionTitle>
        <InformationMessage>
          M.Szell, S. Mimar, T. Perlman, G. Ghoshal, and R. Sinatra <br />{" "}
          Growing Urban Bicycle Networks <br /> arXiv: <LinkStyle href="http://arxiv.org/abs/2107.02185">2107.02185</LinkStyle>
        </InformationMessage>
        <SectionTitle>Source code</SectionTitle>
        <InformationMessage>
          Research:{" "}
          <LinkStyle href="https://github.com/mszell/bikenwgrowth">
            https://github.com/mszell/bikenwgrowth{" "}
          </LinkStyle>{" "}
          <br />
          Visualization:{" "}
          <LinkStyle href={"https://gitlab.com/Lynghede/bikeviz"}>
            https://gitlab.com/Lynghede/bikeviz
          </LinkStyle>
        </InformationMessage>
        <SectionTitle>Team</SectionTitle>
        <ParticipationInformation>
          <InformationMessage style={{ width: "50%" }}>
            <SectionTitle style={{ fontSize: "18px" }}>Research</SectionTitle>
            <LinkStyle href="http://michael.szell.net/">
              Michael Szell,
            </LinkStyle>{" "}
            Lead <br />{" "}
            <RoleText>
              {" "}
              NEtwoRks, Data, and Society (NERDS), <br />
              ITU Copenhagen{" "}
            </RoleText>{" "}
            <br /> <br />
            Sayat Mimar <br /> <RoleText>
              {" "}
              University of Rochester{" "}
            </RoleText>{" "}
            <br /> <br />
            Tyler Perlman <br /> <RoleText>
              {" "}
              University of Rochester{" "}
            </RoleText>{" "}
            <br /> <br />
            <LinkStyle href="http://gghoshal.pas.rochester.edu/">
              {" "}
              Gourab Ghoshal{" "}
            </LinkStyle>{" "}
            <br />
            <RoleText> University of Rochester </RoleText> <br /> <br />
            <LinkStyle href="https://www.robertasinatra.com/">
              Roberta Sinatra
            </LinkStyle>{" "}
            <br />
            <RoleText>
              {" "}
              NEtwoRks, Data, and Society (NERDS), <br />
              ITU Copenhagen{" "}
            </RoleText>
          </InformationMessage>
          <InformationMessage style={{ width: "50%" }}>
            <SectionTitle style={{ fontSize: "18px" }}>
              Visualization
            </SectionTitle>
            Cecilia Laura Kolding Andersen <br />{" "}
            <RoleText>
              {" "}
              Design &amp; Implementation, <br /> ITU Copenhagen{" "}
            </RoleText>
            <br />
            <br />
            Morten Lynghede <br />{" "}
            <RoleText>
              {" "}
              Design &amp; Implementation, <br /> ITU Copenhagen{" "}
            </RoleText>
            <br />
            <br />
            Michael Szell <br />{" "}
            <RoleText>
              {" "}
              Supervision, <br /> ITU Copenhagen{" "}
            </RoleText>
          </InformationMessage>
        </ParticipationInformation>
      </WindowWrapper>
    </>
  );
};
export default InformationAdvanced;

const InformationMessage = styled.div.attrs({
  className: "InformationMessage",
})`
  font-size: 14px;
  color: ${(props) => props.theme.statText};
  padding: 0.5rem;
  width: 100%;
`;

const LinkStyle = styled.a.attrs({
  className: "LinkStyle",
})`
  color: #3e9bdf;
  :hover {
    text-decoration: underline;
  }
`;

const ParticipationInformation = styled.div.attrs({
  className: "ParticipationInformation",
})`
  display: flex;
  flex-direction: row;
  padding: 0.5rem;
  width: 100%;
`;
const RoleText = styled.div.attrs({
  className: "RoleText",
})`
  font-size: 12px;
  color: #989898;
`;

export const SectionTitle = styled.div.attrs({
  className: "SectionTitle",
})`
  font-size: 21px;
  color: #0eb6d2;
  text-align: start;
  margin-bottom: 3px;
`;

const ImageWrapper = styled.div.attrs({ className: "image_wrapper" })`
  position: relative;
  display: flex;
  min-height: 800px;
  min-width: 700px;
  border: ${(p) => p.theme.debugBorder} solid green;
`;

const SeeFigure = styled.button.attrs({ className: "SeeFigure" })`
  color: #3e9bdf;
  background: transparent;
  outline: none;
  padding: 0px;
  margin: 0px;
  border: none;
  :hover {
    text-decoration: underline;
  }
`;

export const NetworkButton = styled.button.attrs({
  className: "NetworkButton",
})`
  outline: none;
  border: 2px solid ${(p) => p.theme.outline};
  color: ${(p) => p.theme.text};
  background: ${(p) => (p.active ? p.theme.grey : "transparent")};
  border-radius: 0.5em;
  padding: 5px;

  :hover {
    background: ${(p) => p.theme.text};
    color: ${(p) => p.theme.background};
  }
`;
