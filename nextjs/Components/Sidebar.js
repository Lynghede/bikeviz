import React, { useState } from "react";
import styled from "styled-components";
// Icons
import { IoIosMenu, IoMdClose } from "react-icons/io";
// Components
import { MenuOption } from "../Components/MenuOption";
import Layers from "./Layers";
import Location from "./Location";
import Filters from "./Filters";

export const Sidebar = (props) => {
  const [sidebar, setSidebar] = useState(false);
  const themeProps = props.themeProps;
  const switchLayer = props.switchLayer;
  const changeStyle = props.changeStyle;
  //const [option, setOption] = useState("Location");

  function handleMenuClick(e) {
    e.preventDefault();
    setSidebar(!sidebar);
  }

  function handleOptionClick(e) {
    e.preventDefault();
    props.setOption(e.currentTarget.value);
  }

  return (
    <>
      {sidebar ? (
        <MenuIcon>
          <IoIosMenu size={30} onClick={handleMenuClick}></IoIosMenu>
        </MenuIcon>
      ) : (
        <SidebarContainer>
          <IoMdClose
            size={20}
            style={{ position: "absolute", top: 5, left: 5 }}
            onClick={handleMenuClick}
          ></IoMdClose>
          <MenuOption
            className="Menu_Option"
            clickOptions={handleOptionClick}
            option={props.option}
          ></MenuOption>
          <SidebarContentWrapper>
            {props.option === "Location" ? (
              <Location themeProps={themeProps} />
            ) : (
              <></>
            )}
            {props.option === "Layer" ? <Layers {...props} /> : <></>}
            {props.option === "Filters" ? (
              <Filters
                switchLayer={switchLayer}
                changeStyle={changeStyle}
                themeProps={themeProps}
              />
            ) : (
              <></>
            )}
          </SidebarContentWrapper>
        </SidebarContainer>
      )}
    </>
  );
};

export default Sidebar;

const SidebarContentWrapper = styled.div.attrs({
  className: "SidebarContentWrapper",
})`
  display: flex;
  flex-direction: column;
  border: ${(p) => p.theme.debugBorder} solid yellow;
  background: ${(p) => p.theme.secondaryDiv};
  padding: 10px;
  height: 100%;
`;

const IconPlacement = styled.div`
  //border: ${(p) => p.theme.debugBorder} solid yellow;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const SidebarContainer = styled.div`
  border: ${(p) => p.theme.debugBorder} solid green;
  height: 75%;
  width: 18%;
  min-width: fit-content;
  position: absolute;
  margin-top: 12px;
  margin-left: 12px;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: ${(p) => p.theme.primaryDiv};
  transition: 0.5s;
  padding-top: 5px;
  display: flex;
  flex-direction: column;
`;

const MenuIcon = styled.div`
  display: inline-block;
  z-index: 1;
  position: relative;
  padding-top: 5px;
  padding-left: 5px;
  padding-top: 5px;
  top: 0;
  left: 0;
`;
