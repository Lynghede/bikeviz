import { Line } from "react-chartjs-2";
import { WindowContainer } from "./Statistics";
import { useState } from "react";

const AdvancedStatistics = (props) => {
  const [hiddenDirectness, setHiddenDirectness] = useState(true);
  const [hiddenLocalEf, setHiddenLocalEf] = useState(true);
  const [hiddenGlobalEf, setHiddenGlobalEf] = useState(true);
  const [hiddenCoverage, setHiddenCoverage] = useState(false);

  function changeHiddenView(e, legendItem, legend) {
    var index = legendItem.datasetIndex;
    if (index === 0) {
      setHiddenDirectness(!hiddenDirectness);
    }
    if (index === 1) {
      setHiddenLocalEf(!hiddenLocalEf);
    }
    if (index === 2) {
      setHiddenGlobalEf(!hiddenGlobalEf);
    } if(index === 3) {
      setHiddenCoverage(!hiddenCoverage);
    }
  }

  //Statistics data
  const statsAdvanced = {
    labels: props.stageSize,
    datasets: [
      {
        label: "Directness",
        fill: false,
        lineTension: 0.5,
        backgroundColor: "rgba(75,192,192,1)",
        borderColor: "rgba(75,192,192,1)",
        borderWidth: 1,
        pointRadius: 1.5,
        pointHoverRadius: 1.5,
        hidden: hiddenDirectness,
        data: props.cityDirectnessMetrics.slice(0, props.slider + 1),
      },
      {
        label: "Local Efficiency",
        fill: false,
        lineTension: 0.5,
        backgroundColor: "rgba(50, 92, 168)",
        borderColor: "rgba(50, 92, 168)",
        borderWidth: 1,
        pointRadius: 1.5,
        pointHoverRadius: 1.5,
        hidden: hiddenLocalEf,
        data: props.cityEfficiencyLocalMetrics.slice(0, props.slider + 1),
      },
      {
        label: "Global Efficiency",
        fill: false,
        lineTension: 0.5,
        backgroundColor: "rgba(145, 140, 255, 1)",
        borderColor: "rgba(145, 140, 255, 1)",
        borderWidth: 1,
        pointRadius: 1.5,
        pointHoverRadius: 1.5,
        hidden: hiddenGlobalEf,
        data: props.cityEfficiencyGlobalMetrics.slice(0, props.slider + 1),
      },
      {
        label: "Coverage",
        fill: false,
        lineTension: 0.5,
        backgroundColor: "rgba(0, 140, 148, 1)",
        borderColor: "rgba(0, 140, 148, 1)",
        borderWidth: 1,
        pointRadius: 1.5,
        pointHoverRadius: 1.5,
        hidden: hiddenCoverage,
        data: props.cityCoverageMetrics.slice(0, props.slider + 1),
      },
    ],
  };

  //Customizing the linechart and tooltips
  const lineChartOptionsAdvancedPercentages = {
    plugins: {
      tooltip: {
        callbacks: {
          label: function (tooltipItem, data) {
            let percentage = "";
            if (tooltipItem.dataset.label === "Coverage") {
              percentage =
                " " +
                tooltipItem.dataset.label +
                ": " +
                (tooltipItem.formattedValue * 100).toFixed(0) +
                "%";
            } else {
            percentage =
              " " +
              tooltipItem.dataset.label +
              ": " +
              (tooltipItem.formattedValue *1).toFixed(2); }
            return [percentage];
          },
          title: function (tooltipItem, data) {
            return "Stage " + tooltipItem[0].label;
          },
        },
      },
      legend: {
        display: true,
        position: "top",
        labels: {
          fontColor: "#323130",
          fontSize: 14,
          boxWidth: 12,
        },
        onClick: changeHiddenView,
      },
    },
    scales: {
      y: {
        position: "left",
        beginAtZero: true,
        suggestedMin: 0,
        suggestedMax: 1,
        ticks: {
          stepSize: 0.1,
          count: 10,
        },
      },
      x: {
        position: "bottom",
        title: {
          display: true,
          text: "Stage",
        },
        ticks: {
          callback: function (label, index, labels) {
            if (label % 5 === 0) {
              return label;
            }
          },
        },
      },
    },
    animation: {
      duration: 0,
    },
  };

  return (
    <>
      <WindowContainer>
        <Line
          data={statsAdvanced}
          options={lineChartOptionsAdvancedPercentages}
          height={"265px"}
          width={"225px"}
        />
      </WindowContainer>
    </>
  );
};

export default AdvancedStatistics;
