import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import styled from "styled-components";
import { cities } from "../public/CityData";
import Link from "next/link";

// Components
import { City } from "./CityList";
import { TitleHeader } from "./Filters";
import { NavIconWrapper } from "./NavigationBar";

// Icons
import { Download as DownloadSVG } from "@styled-icons/fa-solid/";

export function splitCityName(cityName) {
  let city = cityName.split(/(?=[A-Z])/);
  let convertedCityName = "";

  if (city.length > 1) {
    convertedCityName = city[0] + " " + city[1];
    if (convertedCityName == "Manhattan New") {
      convertedCityName = "Manhattan";
    }
    if (convertedCityName == "Manchester Greater") {
      convertedCityName = "Manchester";
    }
  }

  if (city.length === 1) {
    convertedCityName = city[0];
    if (convertedCityName == "Mexico") {
      convertedCityName = "Mexico City";
    }
    if (convertedCityName === "Santiago") {
      convertedCityName = city[0] + " " + "Centro";
    }
  }
  return convertedCityName;
}

export function setCityLink(city) {
  let cityName = city;
  if (city === "manhattannewyork") {
    cityName = "manhattan";
  }
  return cityName;
}

const Location = () => {
  const [allData, setAllData] = useState([]);
  const [filteredData, setFilteredData] = useState([]);
  const router = useRouter();

  useEffect(() => {
    setAllData(cities);
    setFilteredData(cities);
  }, []);

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    value = value.replace(/[^A-Za-z]/gi, "");
    let result = [];
    result = Object.values(allData).filter((data) => {
      let citySearch = data.city.toLowerCase();
      let countrySearch = data.country.toLowerCase();
      let combined = citySearch + countrySearch;
      return combined.search(value) != -1;
    });

    setFilteredData(result);
  };

  function isActive(selected) {
    const toLower = selected.toLowerCase();
    return toLower === router.query.id;
  }

  function setCityLink(city) {
    let cityName = city;
    if (city === "manhattannewyork") {
      cityName = "manhattan";
    }
    return cityName;
  }

  return (
    <OptionWrapper>
      <TitleHeader> Cities </TitleHeader>
      <SearchCities
        type="text"
        placeholder="Search city or country"
        pattern="[a-zA-Z]"
        onChange={(event) => handleSearch(event)}
        required
      ></SearchCities>
      <SelectionWrapper>
        {Object.values(filteredData).map((value, index) => {
          const cityLowerCase = value.city.toLowerCase();
          return (
            <CityAndDownloadWrapper
              border={true}
              active={isActive(value.city)}
              value={value.city}
            >
              <Link
                href={setCityLink(cityLowerCase)}
                key={index}
                prefetch={false}
              >
                <City
                  active={isActive(value.city)}
                  value={value.city}
                  maxWidth={"80%"}
                  as="a"
                >
                  <div style={{ fontSize: "18px", fontWeight: "bold" }}>
                    {splitCityName(value.city)}
                  </div>
                  <div style={{ fontSize: "14px" }}>{value.country}</div>
                </City>
              </Link>
              <OuterNavIconWrapper>
                <NavIconWrapper
                  style={{ margin: "5px" }}
                  marginBottom={"0px"}
                  value="Download"
                  hover={true}
                >
                  <a
                    href={`/download/${cityLowerCase}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <DownloadSVG size={30} />
                  </a>
                </NavIconWrapper>
              </OuterNavIconWrapper>
            </CityAndDownloadWrapper>
          );
        })}
      </SelectionWrapper>
    </OptionWrapper>
  );
};

export default Location;

const OuterNavIconWrapper = styled.div`
  display: flex;
  margin: auto;
  background-color: ${(props) =>
    props.active ? props.theme.grey : props.theme.background};
  border-radius: 50%;

  :hover {
    background: ${(p) => p.theme.background};
  }
`;

const CityAndDownloadWrapper = styled.div.attrs({
  className: "CityAndDownloadWrapper",
})`
  display: flex;
  margin-bottom: 10px;
  background-color: ${(props) =>
    props.active ? props.theme.grey : props.theme.background};
  color: ${(p) => p.theme.text};
  border: ${(p) => (p.border ? "2px solid " + p.theme.outline : "")};
  border-radius: 3px;
  :hover {
    background: ${(p) => p.theme.text};
    color: ${(p) => p.theme.background};
  }
`;

export const OptionWrapper = styled.div.attrs({ className: "optionWrapper" })`
  display: flex;
  flex-direction: column;
  overflow: auto;
  scrollbar-width: none;
`;

export const SearchCities = styled.input.attrs({
  className: "SearchCities",
})`
  padding: 5px;
  min-width: fit-content;
  background: ${(p) => p.theme.grey};
  border: 2px solid ${(p) => p.theme.outline};
  border-radius: 3px;
  width: ${(p) => (p.width ? p.width : "100%")};
  margin-bottom: 10px;
  outline: none;

  ::placeholder {
    color: black;
    opacity: 0.6;
  }

  :hover {
    background: ${(p) => p.theme.grey};
  }
`;

export const SelectionWrapper = styled.div.attrs({
  className: "SelectionWrapper",
})`
  display: flex;
  overflow: auto;
  flex-direction: column;
  scrollbar-width: none;
  border: ${(p) => p.theme.debugBorder} solid white;
`;
