import styled from "styled-components";

export const ImageWrapper = styled.div.attrs({ className: "image_wrapper" })`
  position: relative;
  display: flex;
  height: 100%;
  width: 100%;
  border: ${(p) => p.theme.debugBorder} solid green;
  z-index: 0;

  // &:before {
  //   content: "";
  //   position: absolute;
  //   background: rgba(255, 0, 0, 0.5);
  //   top: 0;
  //   right: 0;
  //   bottom: 0;
  //   left: 0;
  // }
`;
