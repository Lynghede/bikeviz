import styled from "styled-components";
import { useState } from "react";
import { OptionWrapper } from "./Location";

const Filters = (props) => {
  const checked = props.themeProps.theme === "dark";
  const [satelliteChecked, setSatelliteChecked] = useState(false);

  const handleCheckedChange = () => {
    props.themeProps.toggleTheme();
    if (!satelliteChecked) {
      props.switchLayer();
    }
  };
  //The checkValue const ensures that props.changeStyle is not called repeatedly
  const handleChangeStyle = (e) => {
    const checkedValue = e.target.checked;
    setSatelliteChecked(checkedValue);

    if (checkedValue) {
      props.changeStyle("satellite-streets-v11");
      // true if dark
    }
    if (!checkedValue) {
      if (checked) {
        props.changeStyle("dark-v10");
      } else {
        props.changeStyle("streets-v11");
      }
    }
  };

  function handleReset() {
    if (!checked) {
      props.themeProps.toggleTheme();
      setSatelliteChecked(false);
    }
    if (checked) {
      // props.themeProps.toggleTheme();
      setSatelliteChecked(false);
    }
    props.changeStyle("dark-v10");
  }

  return (
    <OptionWrapper>
      <TitleHeader>Filters</TitleHeader>
      <div>
        <CheckBoxWrapper>
          <CheckBox
            onChange={handleCheckedChange}
            checked={checked}
            id="darkmode"
            type="checkbox"
          />
          <CheckBoxLabel htmlFor="darkmode" />
          <p>Dark Mode</p>
        </CheckBoxWrapper>
      </div>
      <div>
        <CheckBoxWrapper>
          <CheckBox
            onChange={handleCheckedChange}
            checked={!checked}
            id="lightmode"
            type="checkbox"
          />
          <CheckBoxLabel htmlFor="lightmode" />
          <p>Light mode</p>
        </CheckBoxWrapper>
      </div>

      <div>
        <CheckBoxWrapper>
          <CheckBox
            onChange={(e) => handleChangeStyle(e)}
            checked={satelliteChecked}
            id="checkbox2"
            type="checkbox"
          />
          <CheckBoxLabel htmlFor="checkbox2" />
          <p>Satellite</p>
        </CheckBoxWrapper>
      </div>

      <ResetButton
        onClick={handleReset}
        disabled={checked && !satelliteChecked}
      >
        Reset Filters
      </ResetButton>
    </OptionWrapper>
  );
};

export default Filters;

export const CheckBoxWrapper = styled.div`
  position: relativ;
  border: ${(p) => p.theme.debugBorder} solid red;
  display: flex;
  align-items: center;
`;

export const CheckBoxLabel = styled.label`
  position: absolute;

  width: 42px;
  height: 26px;
  border-radius: 15px;
  background: ${(p) => p.theme.grey};
  cursor: pointer;
  &::after {
    content: "";
    display: block;
    border-radius: 50%;
    width: 18px;
    height: 18px;
    margin: 3px;
    background: #ffffff;
    box-shadow: 1px 3px 3px 1px rgba(0, 0, 0, 0.2);
    transition: 0.2s;
  }
`;
export const CheckBox = styled.input`
  opacity: 0;
  z-index: 1;
  border-radius: 15px;
  width: 42px;
  height: 26px;
  &:checked + ${CheckBoxLabel} {
    background: #4fbe79;
    &::after {
      content: "";
      display: block;
      border-radius: 50%;
      width: 18px;
      height: 18px;
      margin-left: 21px;
      transition: 0.2s;
    }
  }
`;

export const TitleHeader = styled.h2.attrs({ className: "TitleHeader" })``;

export const ResetButton = styled.button`
  outline: none;
  background: transparent;
  border: 2px solid ${(p) => p.theme.text};
  color: ${(p) => p.theme.text};
  border-radius: 2em;
  margin: 5px 0 5px 0;
  padding: 5px;
  font-size: ${(p) => (p.fontSize ? p.fontSize : "")};

  :hover {
    background: ${(p) => p.theme.grey};
    border: 2px solid ${(p) => p.theme.grey};
  }
`;
