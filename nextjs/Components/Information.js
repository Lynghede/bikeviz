import styled from "styled-components";
import { IoMdClose } from "react-icons/io";
import { WindowWrapper, WindowContainer } from "./Statistics";
import { useState } from "react";
import InformationAdvanced from "./InformationAdvanced";
import { ResetButton } from "./Filters";

const Information = (props) => {
  const [learnMore, setLearnMore] = useState(false);

  function handleLearnMore() {
    setLearnMore(!learnMore);
  }

  return (
    <>
      {!learnMore ? (
        <WindowWrapper
          style={{
            top: "42px",
            minHeight: "200px",
            minWidth: "750px",
            maxWidth: "200px",
          }}
        >
          <IoMdClose
            size={20}
            style={{ position: "absolute", top: 5, left: 5 }}
            onClick={props.clickCross}
          ></IoMdClose>
          <WindowContainer>
            <div style={{ fontSize: "25px", fontWeight: "bold" }}>
              Welcome to GrowBike.Net!
            </div>
          </WindowContainer>
          <InformationMessage>
            Explore how to grow bicycle networks from scratch. <br /> <br/>
            Choose a city and grow the bike network, connecting places
            efficiently step by step. <br /> <br /> 
          </InformationMessage>
          <ResetButton onClick={handleLearnMore}>Learn more</ResetButton>
        </WindowWrapper>
      ) : (
        <InformationAdvanced
          optionLayer={props.optionLayer}
          setOptionLayer={props.setOptionLayer}
          clickCross={props.clickCross}
        />
      )}
    </>
  );
};
export default Information;

const InformationMessage = styled.div.attrs({
  className: "InformationMessage",
})`
  font-size: 16px;
  color: ${(props) => props.theme.statText};
  padding: 0.5rem;
  width: 100%;
`;

