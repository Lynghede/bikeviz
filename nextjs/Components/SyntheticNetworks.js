import styled from "styled-components";

export const SyntheticNetworks = (props) => {
  function isActivePoi(selected) {
    return selected === props.poi;
  }

  function isActiveStrategy(selected) {
    return selected === props.growthStrategy;
  }

  function handlePoiClick(e) {
    e.preventDefault();
    props.setPoi(e.target.value);
  }

  function handleStrategyClick(e) {
    e.preventDefault();
    props.setGrowthStrategy(e.target.value);
  }

  return (
    <SyntheticNetworkWrapper>
      <ButtonWrapper>
        <GridRailButton
          onClick={handlePoiClick}
          value="rail"
          active={isActivePoi("rail")}
        >
          Rail
        </GridRailButton>
        <GridRailButton
          onClick={handlePoiClick}
          value="grid"
          active={isActivePoi("grid")}
        >
          Grid
        </GridRailButton>
      </ButtonWrapper>
      <ButtonWrapper>
        <GridRailButton
          onClick={handleStrategyClick}
          value="bq"
          active={isActiveStrategy("bq")}
        >
          B
        </GridRailButton>
        <GridRailButton
          onClick={handleStrategyClick}
          value="cq"
          active={isActiveStrategy("cq")}
        >
          C
        </GridRailButton>
        <GridRailButton
          onClick={handleStrategyClick}
          value="rq"
          active={isActiveStrategy("rq")}
        >
          R
        </GridRailButton>
      </ButtonWrapper>
    </SyntheticNetworkWrapper>
  );
};

export default SyntheticNetworks;

export const SyntheticNetworkWrapper = styled.div.attrs({
  className: "SyntheticNetworkWrapper",
})`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px;
  opacity: 0.5;
  margin-left: 40px;

  :hover {
    opacity: 1;
  }
`;

export const ButtonWrapper = styled.div.attrs({
  className: "ButtonWrapper",
})`
  display: flex;
  flex-direction: row;
  width: -webkit-fill-available;
  background: ${(p) => p.theme.secondaryDiv};
  border-radius: 0.5em;
  width: 125px;
`;
export const GridRailButton = styled.button.attrs({
  className: "GridRailNetworks",
})`
  outline: none;
  border: 2px solid ${(p) => p.theme.outline};
  color: ${(p) => p.theme.text};
  background: ${(p) => (p.active ? p.theme.grey : "transparent")};
  border-radius: 0.5em;
  padding: 5px;
  width: 100%;

  :hover {
    background: ${(p) => p.theme.text};
    color: ${(p) => p.theme.background};
  }
`;
