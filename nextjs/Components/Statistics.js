import styled from "styled-components";
import { IoMdClose } from "react-icons/io";
import { cities } from "../public/CityData";
import AdvancedStatistics from "./AdvancedStatistics";
import { Line } from "react-chartjs-2";
import {useState} from "react";
import { splitCityName } from "./Location";

const Statistics = (props) => {
  const chosenCity = props.city;
  const cityCapital = chosenCity[0].toUpperCase() + chosenCity.slice(1);
  const directnessMetrics = ["0"];
  const efficiencyLocalMetrics = ["0"];
  const efficiencyGlobalMetrics = ["0"];
  const lengthMetrics = ["0"];
  const coverageMetrics = ["0"];
  const [hiddenLength, setHiddenLength] = useState(false);
  let coveragePercentage = cities[props.city].coveragesqkm;

  function changeLengthHidden(e, legendItem, legend) {
    var index = legendItem.datasetIndex;
    if(index === 0) {
      setHiddenLength(!hiddenLength);
    }
  }

  let stageSize = [
    NaN,
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
  ];

  function calculatePrice(lengthMetric, sliderValue) {
    let estimatedPriceDKK = lengthMetric[sliderValue] * 16.5;
    let estimatedPriceEuro = lengthMetric[sliderValue] * 16.5 * 0.13448;
    if (chosenCity === "copenhagen") {
      return estimatedPriceDKK < 1000
        ? "DKK" + estimatedPriceDKK.toFixed(0) + "million"
        : "DKK" + (estimatedPriceDKK / 1000).toFixed(1) + "billion";
    } else {
      return estimatedPriceEuro < 1000
        ? "€" + estimatedPriceEuro.toFixed(0) + "million"
        : "€" + (estimatedPriceEuro / 1000).toFixed(1) + "billion";
    }
  }

  Object.values(props.stats).map((ele) => {
    directnessMetrics.push(parseFloat(ele.directness_lcc).toFixed(2));
    efficiencyLocalMetrics.push(parseFloat(ele.efficiency_local).toFixed(2));
    efficiencyGlobalMetrics.push(parseFloat(ele.efficiency_global).toFixed(2));
    lengthMetrics.push((parseFloat(ele.length) / 1000).toFixed(0));
    coverageMetrics.push(
      parseFloat(ele.coverage / coveragePercentage).toFixed(2)
    );
  });

  const stateGeneralLength = {
    labels: stageSize,
    datasets: [
      {
        label: "Length",
        fill: false,
        lineTension: 0.5,
        backgroundColor: "rgba(167, 112, 179)",
        borderColor: "rgba(167, 112, 179)",
        borderWidth: 1,
        pointRadius: 1.5,
        pointHoverRadius: 1.5,
        hidden: hiddenLength,
        data: lengthMetrics.slice(0, props.slider + 1),
      },
    ],
  };

  const lineChartOptionsGeneralLength = {
    aspectRatio: 1,
    plugins: {
      tooltip: {
        callbacks: {
          label: function (tooltipItem, data) {
            let lengthMetric =
              " Length: " + lengthMetrics[tooltipItem.label] + "km";
            let priceEstimate =
              " Estimated price: " +
              calculatePrice(lengthMetrics, tooltipItem.label);
            return [lengthMetric, priceEstimate];
          },
          title: function (tooltipItem, data) {
            return "Stage " + tooltipItem[0].label;
          },
        },
      },
      legend: {
        display: true,
        position: "top",
        labels: {
          fontColor: "#323130",
          fontSize: 14,
          boxWidth: 12,
        },
        onClick: changeLengthHidden,
      },
    },
    scales: {
      y: {
        position: "left",
        min: 0,
        suggestedMax: lengthMetrics[40],
        ticks: {
          callback: function (label, index, labels) {
            if (Math.floor(label) === label) {
              return label;
            }
          },
        },
      },
      x: {
        position: "bottom",
        ticks: {
          callback: function (label, index, labels) {
            if (label % 5 === 0) {
              return label;
            }
          },
        },
      },
    },
    animation: {
      duration: 0,
    },
  };

  return (
    <>
      <WindowWrapper maxWidth={"400px"} minWidth={"275px"}>
        <IoMdClose
          size={20}
          style={{ position: "absolute", top: 5, left: 5 }}
          onClick={props.clickCross}
        ></IoMdClose>
        <WindowContainer>
          <div style={{ fontSize: "15px", marginBottom: "5px", }}>
            {splitCityName(props.statsCity)}
          </div>
          <div>Stage {props.slider}</div>
          <Line
            data={stateGeneralLength}
            options={lineChartOptionsGeneralLength}
            height={"225px"}
            width={"225px"}
          />
          <AdvancedStatistics
            city={chosenCity}
            cityCoverageMetrics={coverageMetrics}
            cityDirectnessMetrics={directnessMetrics}
            cityEfficiencyLocalMetrics={efficiencyLocalMetrics}
            cityEfficiencyGlobalMetrics={efficiencyGlobalMetrics}
            slider={props.slider}
            coveragePercentage={coveragePercentage}
            stageSize={stageSize}
          />
        </WindowContainer>
      </WindowWrapper>
    </>
  );
};

export default Statistics;

export const WindowWrapper = styled.div.attrs({
  className: "WindowWrapper",
})`
  display: flex;
  min-width: ${(p) => (p.minWidth ? p.minWidth : "0px")};
  max-width: ${(p) => (p.maxWidth ? p.maxWidth : "300px")};
  min-height: 300px;
  max-height: 600px;
  background: ${(p) => p.theme.secondaryDiv};
  position: absolute;
  align-items: center;
  flex-direction: column;
  padding: 10px;
  top: 0;
  right: 3em;
`;

export const WindowContainer = styled.div.attrs({
  className: "WindowContainer",
})`
  display: flex;
  justify-content: center;
  flex-direction: column;
  text-align: center;
  margin-top: 5px;
  margin-bottom: 5px;
  font-size: 12px;
  max-height: 600px;
`;
