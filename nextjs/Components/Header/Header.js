import Link from "next/link";

import {
  Header,
  Wrapper,
  Container,
} from "../Documentation/Documentation.styled";
import Icon from "../Icon";

export function HeaderTop(props) {
  const href = props.href ? props.href : "/";
  const iconDisplay = props.icon;
  const sizeIcon = props.sizeIcon;
  const marginIcon = props.margin ? props.margin : "10px";

  return (
    <Header>
      <Container style={{ justifyContent: "space-between" }}>
        <Link href={href} passHref>
          <Wrapper as="a">
            <Icon icon={iconDisplay} margin={marginIcon} sizeIcon={sizeIcon} />
            <h1>
              {" "}
              <span>{props.routeTitle}</span> | {props.urltitle}
            </h1>
          </Wrapper>
        </Link>
        <Link href="/" passHref>
          <Wrapper as="a">
            <h1>| GrowBike.Net</h1>
          </Wrapper>
        </Link>
      </Container>
    </Header>
  );
}
