import React from "react";
import {
  IoFunnelOutline,
  IoLogoBuffer,
  IoLocationOutline,
} from "react-icons/io5";
import styled from "styled-components";
import Tooltip from "@material-ui/core/Tooltip";

export const MenuOption = (props) => {
  function isActive(selected) {
    return selected === props.option;
  }

  return (
    <IconContainer>
      <Tooltip title="Location" placement="top" arrow>
      <IconButtonWrapper
        active={isActive("Location")}
        value="Location"
        onClick={props.clickOptions}
      >
        <IoLocationOutline size={25} />
      </IconButtonWrapper>
      </Tooltip>
      <Tooltip title="Layers" placement="top" arrow>
      <IconButtonWrapper
        active={isActive("Layer")}
        value="Layer"
        onClick={props.clickOptions}
      >
        <IoLogoBuffer size={25} />
      </IconButtonWrapper>
      </Tooltip>
      <Tooltip title="Filters" placement="top" arrow>
      <IconButtonWrapper
        active={isActive("Filters")}
        value="Filters"
        onClick={props.clickOptions}
      >
        <IoFunnelOutline size={25} />
      </IconButtonWrapper>
      </Tooltip>
    </IconContainer>
  );
};

export default MenuOption;

const IconButtonWrapper = styled.button`
  background: ${(p) => p.theme.primaryDiv};
  border: none;
  outline: none;
  border-bottom: ${(props) =>
    props.active ? "2px solid " + props.theme.text : "none"};
  padding-bottom: 4px;
  margin-top: 5px;
  display: flex;
  width: 100%;
  justify-content: center;
`;

const IconContainer = styled.div`
  border: ${(p) => p.theme.debugBorder} solid blue;
  width: 100%;
  margin-top: 20px;
  z-index: 1;
  background-color: ${(p) => p.theme.primaryDiv};
  display: inline-block;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  svg {
    color: ${(p) => p.theme.text};
  }
`;