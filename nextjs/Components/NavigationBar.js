import React, { useState } from "react";
import styled from "styled-components";
import {
  IoBarChart,
  IoDocumentTextOutline,
  IoInformationCircleOutline,
} from "react-icons/io5";
import Tooltip from "@material-ui/core/Tooltip";
import Statistics from "./Statistics";
import Information from "./Information";
import { AiOutlineLineChart } from "react-icons/ai";
import { Download as DownloadSVG } from "@styled-icons/fa-solid/";

const NavigationBar = (props) => {
  const [option, setOption] = useState("Information");

  function handleIconClick(e) {
    e.preventDefault();
    if (e.currentTarget.value === option) {
      setOption("");
    } else {
      setOption(e.currentTarget.value);
    }
  }

  function handleCloseClick(e) {
    e.preventDefault();
    setOption("");
  }

  function isActive(selected) {
    return selected === option;
  }
  return (
    <NavigationOuterDiv>
      {option === "Statistics" ? (
        <Statistics
          city={props.city}
          statsCity={props.statsCity}
          stats={props.stats}
          slider={props.slider}
          clickCross={handleCloseClick}
          themeProps={props.themeProps}
        />
      ) : (
        <></>
      )}
      {option === "Information" ? (
        <Information
          clickCross={handleCloseClick}
          optionLayer={props.optionLayer}
          setOptionLayer={props.setOptionLayer}
        />
      ) : (
        <></>
      )}

      <NavigationContainer>
        <Tooltip title="Network Statistics" placement="left" arrow>
          <NavIconWrapper
            active={isActive("Statistics")}
            value="Statistics"
            onClick={handleIconClick}
          >
            <AiOutlineLineChart size={27} />
          </NavIconWrapper>
        </Tooltip>
        <Tooltip title="Information" placement="left" arrow>
          <NavIconWrapper
            active={isActive("Information")}
            value="Information"
            onClick={handleIconClick}
          >
            <IoInformationCircleOutline size={27} />
          </NavIconWrapper>
        </Tooltip>
        <Tooltip title="Download" placement="left" arrow>
          <NavIconWrapper value="Download">
            <a href={`/download`} target="_blank" rel="noopener noreferrer">
              <DownloadSVG size={24} />
            </a>
          </NavIconWrapper>
        </Tooltip>
      </NavigationContainer>
    </NavigationOuterDiv>
  );
};

export default NavigationBar;

const NavigationOuterDiv = styled.div.attrs({
  className: "NavigationOuterDiv",
})`
  position: absolute;
  top: 5px;
  right: 2px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  background: ${(p) => p.theme.secondaryDivOpacity};
  border-radius: 5%;
`;

export const NavIconWrapper = styled.button`
  background: transparent;
  color: ${(p) => p.theme.text};
  margin-bottom: ${(p) => (p.marginBottom ? p.marginBottom : "5px")};
  outline-color: black;
  border: ${(props) =>
    props.active
      ? "2px solid " + props.theme.outline
      : "2px solid transparent"};
  border-radius: ${(props) => (props.active ? "4px" : "none")};

  :hover {
    opacity: 70%;
    color: ${(p) => (p.hover ? p.theme.highlight : p.theme.text)};
  }
`;

const NavigationContainer = styled.div.attrs({
  className: "NavigationContainer",
})`
  position: relative;
  display: flex;
  flex-direction: column;

  svg {
    margin-bottom: 2px;
    margin-top: 2px;
    color: ${(p) => p.theme.text};
  }
`;
