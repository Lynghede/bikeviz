const { MongoClient } = require("mongodb");

// Replace the uri string with your MongoDB deployment's connection string.
const uri =
  "mongodb+srv://moly:108391vest@cluster0.p2oqg.mongodb.net/cities?retryWrites=true&w=majority";

const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

async function run() {
  try {
    await client.connect();

    const database = client.db("cities");
    const cities = database.collection("paris");

    // Query for a movie that has the title 'Back to the Future'
    const query = { paris: "paris" };
    const city = await cities.findOne(query);

    console.log(city);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);
