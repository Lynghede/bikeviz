import fs from "fs";
import path from "path";

const POST_FOLDER_NAME = "Images";

const POSTS_DIRECTORY = path.join(process.cwd(), "public", POST_FOLDER_NAME);

export const getImagesRoutes = (id) => {
  const listOfRoutes = fs.readdirSync(`${POSTS_DIRECTORY}/${id}`);

  return listOfRoutes;
};

const VIDEO_FOLDER_NAME = "Videos";
const VIDEO_DIRECTORY = path.join(process.cwd(), "public", VIDEO_FOLDER_NAME);

/**
 *
 * @param {*The id string is the name of the city provided by getStaticPaths} id
 * @returns A list of all files in the videos folder
 */
export const getVideoRoutes = (id) => {
  const listOfRoutes = fs.readdirSync(`${VIDEO_DIRECTORY}/${id}`);
  return listOfRoutes;
};

export function getAllCitiesIds() {
  const fileNames = fs.readdirSync(POSTS_DIRECTORY);

  return fileNames.map((fileName) => {
    return {
      id: fileName,
    };
  });
}
