export async function measureTime(name, func, n = 100) {
  const times = [];

  for (let i = 0; i < n; i++) {
    const t0 = performance.now();
    await func;
    const t1 = performance.now();
    times.push(t1 - t0);
  }
  // loop end
  const reducer = (accum, currentVal) => accum + currentVal;
  let avg = times.reduce(reducer, 0);
  avg = avg / n;
  console.log(name + " took " + avg / 1000 + " seconds");
}
