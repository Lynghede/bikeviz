import { useState, useEffect } from "react";

export const useDarkMode = () => {
  const [theme, setTheme] = useState(
    process.browser ? window.localStorage.getItem("theme") : "dark"
  );

  const setMode = (mode) => {
    window.localStorage.setItem("theme", mode);
    setTheme(mode);
  };

  useEffect(() => {
    if (theme === null) {
      setMode("dark");
    }
  }, [theme]);

  const toggleTheme = () => {
    if (theme === "light") {
      setMode("dark");
    } else {
      setMode("light");
    }
  };

  return [theme, toggleTheme];
};

export const useOnClickOutside = (ref, handler) => {
  useEffect(() => {
    const listener = (event) => {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }
      handler(event);
    };
    document.addEventListener("mousedown", listener);
    return () => {
      document.removeEventListener("mousedown", listener);
    };
  }, [ref, handler]);
};
