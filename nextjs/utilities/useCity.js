import { fetcher } from "./fetcher";
import useSWR from "swr";

function useCity(id) {
  const { data, error } = useSWR(`../pages/api/get_city/${id}`, fetcher);

  return {
    city: data,
    isLoading: !error && !data,
    isError: error,
  };
}
