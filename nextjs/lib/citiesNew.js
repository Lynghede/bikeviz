const dev = process.env.NODE_ENV !== "production";
export const server = dev
  ? "http://localhost:3000/"
  : "https://bicyclevisualization.vercel.app/";

export async function getCityData(id) {
  const idUpperCase = id.toUpperCase();
  const responsePath = await fetch(
    `${server}Cities/${id}/${id}/${idUpperCase}.json`
  );
  const responsePointer = await fetch(
    `${server}Cities/${id}/${id}Markers/${id}_poi_railwaystation.json`
  );
  const responseStatistics = await fetch(
    `${server}Cities/${id}/statistics/${id}.json`
  );
  const responseExisting = await fetch(
    `${server}Cities/${id}/existing/${id}biketrack.json`
  );

  const path = await responsePath.json(); // is Promise
  const pointer = await responsePointer.json();
  const stats = await responseStatistics.json();
  const existing = await responseExisting.json();

  return {
    id,
    path,
    pointer,
    stats,
    existing,
  };
}
