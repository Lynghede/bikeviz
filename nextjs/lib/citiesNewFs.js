import fs from "fs";
import path from "path";

export async function getCityDataFS(id) {
  const publicDir = path.join(process.cwd(), "public");

  const idUpperCase = id.toUpperCase();

  const responsePath = fs.readFileSync(
    `${publicDir}/Cities/${id}/${id}/${idUpperCase}.json`,
    "utf8"
  );
  const responsePointer = fs.readFileSync(
    `${publicDir}/Cities/${id}/${id}Markers/${id}_poi_railwaystation.json`,
    "utf8"
  );
  const responseStatistics = fs.readFileSync(
    `${publicDir}/Cities/${id}/statistics/${id}.json`,
    "utf8"
  );

  const responseExisting = fs.readFileSync(
    `${publicDir}/Cities/${id}/existing/${id}biketrack.json`,
    "utf8"
  );
  const pathOut = JSON.parse(responsePath); // is Promise
  const pointer = JSON.parse(responsePointer);
  const stats = JSON.parse(responseStatistics);
  const existing = JSON.parse(responseExisting);

  return {
    id,
    path: pathOut,
    pointer,
    stats,
    existing,
  };
}
