import os
import csv
import json
from geojson import Feature, FeatureCollection, Point

# Should use dataForWrangling dir instead.
#dirPath = "../dataReady/tokyo/poi"
#os.chdir(dirPath)

def poiWrangler(fileInput: str, fileOutput: str, dirOutput: str):

    features = []
    with open(f'{fileInput}', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for latitude, longitude in reader:
            latitude, longitude = map(float, (latitude, longitude))
            features.append(
                Feature(
                    geometry = Point((longitude, latitude)),
                )
            )

    collection = FeatureCollection(features)
    resultsFolder = f"../dataReady/{dirOutput}/"
    with open(f"{resultsFolder}{fileOutput}.json", "w") as f:
        f.write('%s' % collection)


# Dict to store all ID's