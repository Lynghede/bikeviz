import os
from pathlib import Path

dirPath = "../dataForWrangling/"
# dirPath = "../../../databikeviz"
parent_dir = "../../nextjs/public/"

def getListOfDir(path: str) -> list:
    os.chdir(path)
    listOfDirNames = []
    for f in os.listdir():
        if not f.startswith('.'): #To avoid .DS_Store file
            listOfDirNames.append(f)
    return listOfDirNames

def createDirs(listOfDirNames: list, parent_dir: str):
    folderType = ["Videos", "Images"]
    for dirName in folderType: # For each folderType
        for city in listOfDirNames: # for each city create its own dir in folderType
            dir = dirName + f"/{city}"
            final_path = os.path.join(parent_dir, dir)
            if not os.path.exists(final_path):
                print(final_path)
                os.makedirs(final_path)
       




files = getListOfDir(dirPath)

print(files)

createDirs(listOfDirNames=getListOfDir(dirPath), parent_dir=parent_dir)