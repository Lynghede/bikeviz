import os
import json
import shutil
from geojsonConverter import write_to_geojson
from poiwrangler import poiWrangler
from statswrangler import statsWrangler


dirPath = "../dataForWrangling/"
# dirPath = "../../../databikeviz"

def get_immediate_subdirectories(a_dir):
    return [f.path for f in os.scandir(a_dir) if f.is_dir()]

def get_rail_cq(file): # Return list of files ending with json
    return [f for f in os.listdir(file) if f.endswith("json")]

def read_json_file(file_path):
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

#print(get_immediate_subdirectories(dirPath))

def dataWrapping(listOfFiles: list, growthType: str, growthStrat: str):
    allDict = {}
    counter = 0
    arrayOfDictStages = []
    stageCount = 1

    # This is one of many files
    # amsterdam_GTbonly_poi_railwaystation_Rq0.025.json
    for file in listOfFiles:
        if file.endswith(".json"):
            file_path = f"{file}"
                        
            placeholder_nameDir = file_path.split("/")[2]
            #print("placeHoldeNameDir: " + placeholder_nameDir)
            name = placeholder_nameDir + "_" + growthType + "_" + growthStrat

            nameDir = placeholder_nameDir + "/" + growthType + "/" + growthStrat
            #print("name: " + nameDir)
                      

            stageDict = {}

            data = read_json_file(file_path)
            #print(data)
            for obj in data["geometries"]: # Check for objects in the column "geometries"
                if obj["type"] == "LineString": # Only add if type "LineString"
                    minLat = min(obj["coordinates"])
                    maxLat = max(obj["coordinates"])
                    
                    id = f'{minLat}{maxLat}'
                    
                    if id not in allDict:
                        allDict[id] = obj["coordinates"]
                        stageDict[id] = obj["coordinates"]
                        #print("not in allDict")

            arrayOfDictStages.append(stageDict)
            
            if len(arrayOfDictStages) == 10:
                counter = counter + 1
                write_to_geojson(arrayOfDictStages, counter, stageCount, name, nameDir)
                stageCount += 10
                #print(arrayOfDictStages)
                arrayOfDictStages = []




#print(get_immediate_subdirectories(dirPath))

for file in get_immediate_subdirectories(dirPath):
    city_name = file.split("/")[2]
    #if file.endswith("london"): # to not print everything
    print(file)
    #pathOfEachFile = get_immediate_subdirectories(file)
    #print(pathOfEachFile)
    rail_rq_list = []
    rail_bq_list = []
    rail_cq_list = []
    grid_rq_list = []
    grid_bq_list = []
    grid_cq_list = []
    # Here we are inside a city folder accessing all the files
    all_list_files = os.listdir(file)
    json_list_files = get_rail_cq(file)
    sorted_list = sorted(json_list_files) # lists all items in the dir
    for ele in sorted_list:
        
        # yolo = os.path.join(file, ele)
        # print("this is yolo:")
        # print(yolo)
        # Here we search for files with railwaystation
        if "railwaystation" in ele:  
            #print(ele)
            if "Rq" in ele:
                file_name_rq = os.path.join(file, ele)
                rail_rq_list.append(file_name_rq)
                #print(ele)

            if "Cq" in ele:
                file_name_cq = os.path.join(file, ele)
                rail_cq_list.append(file_name_cq)
                # do something
                #print("cq")
            if "Bq" in ele:
                file_name_bq = os.path.join(file, ele)
                rail_bq_list.append(file_name_bq)
                # do something
                #print("bq")
        # Here we search for files with grid
        if "grid" in ele:
            #print(ele)
            if "Rq" in ele:
                file_name_rq = os.path.join(file, ele)
                grid_rq_list.append(file_name_rq)
                #print(ele)
            if "Cq" in ele:
                file_name_cq = os.path.join(file, ele)
                grid_cq_list.append(file_name_cq)
                # do something
                #print("cq")
            if "Bq" in ele:
                file_name_bq = os.path.join(file, ele)
                grid_bq_list.append(file_name_bq)
                # do something
                #print("bq")
        if f"{city_name}_biketrack.json" in ele:
            #print(ele)
            dirFrom = f'{file}/{ele}'
            dirToo = f'../dataReady/{city_name}/existing/'
            dest_file = shutil.move(dirFrom, dirToo)
    for ele in all_list_files: # Handling CSV files
        #     print("")
        if ele.endswith("csv"):
            #print("")
                
            if "grid" in ele:
                growthType = "Grid"
                fileInput = os.path.join(file, ele)    
                # Here we search for the poi for grid
                if "latlon" in ele: # Find latlon grid
                    #print(ele) # = amsterdam_poi_grid_latlon.csv
                    #print(file) # = ../dataForWrangling/amsterdam - this should be the fileInput
                    fileInput = os.path.join(file, ele) # ../dataForWrangling/amsterdam/amsterdam_poi_grid_latlon.csv
                    dirOutput = f'{city_name}/poi'
                    poiWrangler(fileInput=fileInput, fileOutput=f"{city_name}_poi_grid", dirOutput=dirOutput)
                if "betweenness" in ele:
                    dirOutput = f'{city_name}/stats{growthType}/bq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_bq', dirOutput=dirOutput)
                if "closeness" in ele:
                    dirOutput = f'{city_name}/stats{growthType}/cq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_cq', dirOutput=dirOutput)
                if "random" in ele:
                    dirOutput = f'{city_name}/stats{growthType}/rq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_rq', dirOutput=dirOutput)
            if "rail" in ele:
                #print(ele)
                fileInput = os.path.join(file, ele)
                #print("first if : " + fileInput)
                if "latlon" in ele:
                    #fileInput = os.path.join(file, ele)
                    dirOutput = f'{city_name}/poi'
                    poiWrangler(fileInput=fileInput, fileOutput=f"{city_name}_poi_rail", dirOutput=dirOutput)
                # statistics wrangling
                if "betweenness" in ele:
                    #fileInput = os.path.join(file, ele)
                    dirOutput = f'{city_name}/statsRail/bq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_bq', dirOutput=dirOutput)
                if "closeness" in ele:
                    dirOutput = f'{city_name}/statsRail/cq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_cq', dirOutput=dirOutput)
                if "random" in ele:
                    dirOutput = f'{city_name}/statsRail/rq'
                    statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_stats_rq', dirOutput=dirOutput)
            if "existing" in ele:
                fileInput = os.path.join(file, ele)
                dirTest = f'{city_name}/existing'
                # print("LLEEE GOOO")
                # print("DIROUTPUT GIVEN TO : " + dirTest)

                # print(fileInput)
                # print(f'yolo motherfucker {city_name}_existing')
                statsWrangler(fileInput=fileInput, fileOutput=f'{city_name}_existing', dirOutput=dirTest)

                print(ele)

    #print(rail_rq_list)
    # for ele in foo_list:
    #     print(ele)

    # DATA WRAPPING ALL NETWORK GROWTH STRATEGIES
    dataWrapping(rail_rq_list, "rail", "rq")
    dataWrapping(rail_bq_list, "rail", "bq")
    dataWrapping(rail_cq_list, "rail", "cq")
    dataWrapping(grid_rq_list, "grid", "rq")
    dataWrapping(grid_bq_list, "grid", "bq")
    dataWrapping(grid_cq_list, "grid", "cq")
        