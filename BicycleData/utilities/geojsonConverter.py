import os
import json
import typing

def read_json_file(file_path):
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

def create_geojson_dict(arr: list, stageCount: int) -> dict:
    gjsonDict = {}
    # gjsonDict["type"] = "FeatureCollection"
    gjsonDict["features"] = []
  
    for count, array_ele in enumerate(arr, start=stageCount):
        stage_arr = []
        for key in array_ele:
            value = array_ele.get(key)
            stage_arr.append({"type" : "LineString", "coordinates" : value})
        
        gjsonDict["features"].append([{"type" : "Feature", "properties" : {"stage" : count}, "geometry" : {"type": "GeometryCollection", "geometries" : stage_arr}}] )
    return gjsonDict

def write_to_geojson(arr: list, stage: int, stageCount: int, fileName: str, dir: str):
    resultsFolder = "../dataReady/" + dir + '/'
    
    with open(f'{resultsFolder}{fileName}{stage}.json', 'w') as outfile:
        json.dump(create_geojson_dict(arr, stageCount), outfile, indent=4)

