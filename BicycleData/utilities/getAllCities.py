import os

dirPath = "../dataForWrangling/"

def getListOfDir(path: str) -> list:
    os.chdir(path)
    listOfDirNames = []
    for f in os.listdir():
        if not f.startswith('.'): #To avoid .DS_Store file
            listOfDirNames.append(f)
    return listOfDirNames


#print(getListOfDir(dirPath))

def createCityDataDict(listOfCities: list):
    cityDataDict = {}
    for city in listOfCities:
        cityDataDict
