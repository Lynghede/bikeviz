import csv
import json

def statsWrangler(fileInput: str, fileOutput: str, dirOutput: str):
    data = []
    resultsFolder = f"../dataReady/{dirOutput}/"
    # print("statsWrangler: " + resultsFolder)
    # print("diroutput in statsWrangler: " + dirOutput)

    with open(fileInput) as csvFile:
        csvReader = csv.DictReader(csvFile)
        for rows in csvReader:
            data.append(rows)
          
    with open(f'{resultsFolder}{fileOutput}.json', 'w') as jsonFile:
        jsonFile.write(json.dumps(data, indent=4))