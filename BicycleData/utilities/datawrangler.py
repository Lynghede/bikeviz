import os
import json
from geojsonConverter import write_to_geojson

#dirPath = "../dataForWrangling/copenhagen/rail/rq"
dirPath = "../dataForWrangling/"
#os.chdir(dirPath)

# Should use dataForWrangling dir instead.

def read_json_file(file_path):
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

def get_immediate_subdirectories(a_dir):
    return [f.path for f in os.scandir(a_dir) if f.is_dir()]

# files = os.listdir()
# sorted_files = sorted(files)
# sorted_files.sort(key=lambda f: os.stat(f).st_size, reverse=False)
#print(sorted_files)

# Dict to store all ID's
allDict = {}
counter = 0
arrayOfDictStages = []
stageCount = 1 # We start the count at 1 because we are artificially constructing an empty stage 0 when fetching the data.

for path in get_immediate_subdirectories(dirPath): # Here we are in ../dataWrangling/[city]
    subDir = get_immediate_subdirectories(path) # list
    for dir in subDir: # Here we are in ../dataWrangling/[city]/..
        if dir.endswith("grid") or dir.endswith("rail"):
        # We want to enter grid and rail
            dir_final = get_immediate_subdirectories(dir)
            for file_initial in dir_final:
                files = os.listdir(file_initial)
                nameSplit = file_initial.split("/")
                name = nameSplit[2] + "_" + nameSplit[3] + "_" + nameSplit[4]
                nameSplitDir = file_initial.split("/", 2)
                nameDir = nameSplitDir[2]
                #print(name)
                #print(files)
                #print(file_initial)
                #print(nameDir)
                
                sorted_files = sorted(files)
                for sorted_file in sorted_files:
                    if os.path.isdir(file_initial):
                        #print("True")
                        final_file_path = os.path.join(file_initial, sorted_file)
                        
                #sorted_files.sort(key=lambda f: os.stat(f).st_size, reverse=False)

                #for file in sorted_files:
                        if final_file_path.endswith(".json"):
                            file_path = f"{final_file_path}"
                            print(file_path)
                            
                            stageDict = {}

                            data = read_json_file(file_path)
                            #print(data)
                            for obj in data["geometries"]: # Check for objects in the column "geometries"
                                if obj["type"] == "LineString": # Only add if type "LineString"
                                    minLat = min(obj["coordinates"])
                                    maxLat = max(obj["coordinates"])
                                    
                                    id = f'{minLat}{maxLat}'
                                    
                                    if id not in allDict:
                                        allDict[id] = obj["coordinates"]
                                        stageDict[id] = obj["coordinates"]
                                        #print("not in allDict")

                            arrayOfDictStages.append(stageDict)
                            if len(arrayOfDictStages) == 10:
                                counter = counter + 1
                                write_to_geojson(arrayOfDictStages, counter, stageCount, name, nameDir)
                                stageCount += 10
                                arrayOfDictStages = []
                counter = 0
                stageCount = 1
                allDict = {}


# Convert each element in arrayOfDictStages to one geojson file, with new property to differ between stages

#write_to_geojson(arrayOfDictStages)
            


        
