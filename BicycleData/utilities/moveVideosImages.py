import os
import shutil

dirPath = "../dataForWrangling"

def get_immediate_subdirectories(a_dir):
    return [f.path for f in os.scandir(a_dir) if f.is_dir()]


#print(get_immediate_subdirectories(dirPath))

listOfVideosImages = get_immediate_subdirectories(dirPath)
#print(listOfVideosImages)

def moveVideosImages(listOfFiles: list):
    for file in listOfFiles:
        city_name = file.split("/")[2]
        dirTooVideos = f"../../nextjs/public/Videos/{city_name}"
        dirTooImages = f"../../nextjs/public/Images/{city_name}"
        filesInDir = os.listdir(file)
        #print(filesInDir)
        for files in filesInDir:
            #print(files)
            if files.endswith(".mp4"):
                print(files)
                dirFrom = f'{file}/{files}'
                dest_file = shutil.move(dirFrom, dirTooVideos)
            if files.endswith(".webm"):
                dirFrom = f'{file}/{files}'
                print("here we go")
                dest_file = shutil.move(dirFrom, dirTooVideos)

            if files.endswith(".png"):
                dirFrom = f'{file}/{files}'
                print(files)
                dest_file = shutil.move(dirFrom, dirTooImages)

                


moveVideosImages(listOfVideosImages)