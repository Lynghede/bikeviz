import os
import json
from pathlib import Path

dirPath = "../dataForWrangling/"
# dirPath = "../../../databikeviz"
parent_dir = "../dataReady/"

def getListOfDir(path: str) -> list:
    os.chdir(path)
    listOfDirNames = []
    for f in os.listdir():
        if not f.startswith('.'): #To avoid .DS_Store file
            listOfDirNames.append(f)
    return listOfDirNames

def createDirs(listOfDirNames: list, parent_dir: str):
    growthStrategies = ["bq", "rq", "cq"]
    growthType = ["rail", "grid", "statsRail", "statsGrid"]
    otherDirs = ["existing", "poi"]
    for dirName in listOfDirNames: # For each city
        for dir in otherDirs:
            otherDirPaths = dirName + f"/{dir}"
            final_otherDP = os.path.join(parent_dir, otherDirPaths)
            if not os.path.exists(final_otherDP):
                os.makedirs(final_otherDP)
                print(final_otherDP)
        for type in growthType: # For each type: rail and grid
            for ele in growthStrategies: # For each growth type: bq, rq, cq
                gridDir = dirName + f"/{type}/{ele}"
                final_path = os.path.join(parent_dir, gridDir)
                if not os.path.exists(final_path):
                    print(final_path)
                    os.makedirs(final_path)
       




files = getListOfDir(dirPath)

#print(files)

createDirs(listOfDirNames=getListOfDir(dirPath), parent_dir=parent_dir)