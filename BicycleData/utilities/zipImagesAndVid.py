
from zipfile import ZipFile
import os
from os.path import basename
import shutil
from pathlib import Path


# create a ZipFile object
def get_immediate_subdirectories(a_dir):
    return [f.path for f in os.scandir(a_dir) if f.is_dir()]

listOfCitiesPathsImg = get_immediate_subdirectories("../../nextjs/public/Images")
listOfCitiesPathsVid = get_immediate_subdirectories("../../nextjs/public/Videos")
# print(listOfCitiesPaths)


def createAllZip(listOfFiles: list):
    for file in listOfFiles:
            #print(file)
            city_name = file.split("/")[5] ## Each city name
            file_type = file.split("/")[4]
            dirTooVideos = f"../../nextjs/public/Videos/{city_name}"
            dirTooImages = f"../../nextjs/public/Images/{city_name}"
            if file_type == "Images":
                abs_path = Path(f"{dirTooImages}/{city_name}.zip")
               
                if abs_path.exists(): # Checks if the file already exists
                        os.remove(abs_path) # Remove file
                        createZipImg(file, city_name) # Create zip
                        dest_file = shutil.move(f"{city_name}.zip", dirTooImages) # Move new zip   
                else:
                    createZipImg(file, city_name) # Create zip
                    dest_file = shutil.move(f"{city_name}.zip", dirTooImages) # Move Zip

            if file_type == "Videos":
                abs_path = Path(f"{dirTooVideos}/{city_name}.zip")
                if abs_path.exists():
                    print("File already exists")
                else:
                    createZipVid(file, city_name)
                    dest_file = shutil.move(f"{city_name}.zip", dirTooVideos)
            #print(city_name)


def createZipVid(pathToWalk: str, zipName: str):
    with ZipFile(f'{zipName}.zip', 'w') as zipObj:
    # Iterate over all the files in directory
        for folderName, subfolders, filenames in os.walk(pathToWalk):
            for filename in filenames:
                #create complete filepath of file in directory
                if filename.endswith(".mp4"):
                    filePath = os.path.join(folderName, filename)
                    print(filename)
                # Add file to zip
                    zipObj.write(filePath, basename(filePath))

def createZipImg(pathToWalk: str, zipName: str):
    with ZipFile(f'{zipName}.zip', 'w') as zipObj:
    # Iterate over all the files in directory
        for folderName, subfolders, filenames in os.walk(pathToWalk):
            for filename in filenames:
                #create complete filepath of file in directory
                
                filePath = os.path.join(folderName, filename)
                print(filename)
            # Add file to zip
                zipObj.write(filePath, basename(filePath))

#createZip()
createAllZip(listOfCitiesPathsImg)
createAllZip(listOfCitiesPathsVid)