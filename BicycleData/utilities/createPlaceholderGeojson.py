import os
import json
import typing

dirPath = "../dataForWrangling/"

def get_immediate_subdirectories(a_dir):
    return [f.path for f in os.scandir(a_dir) if f.is_dir()]

def read_json_file(file_path):
    with open(file_path) as json_file:
        data = json.load(json_file)
        return data

def create_geojson_dict(arr: list, stageCount: int) -> dict:
    gjsonDict = {}
    # gjsonDict["type"] = "FeatureCollection"
    gjsonDict["features"] = []
    for i in range(31, 40):
    #for count, array_ele in enumerate(arr, start=stageCount):
        stage_arr = []
        
        #value = array_ele.get(key)
        stage_arr.append({"type" : "LineString", "coordinates" : []})
        
        gjsonDict["features"].append([{"type" : "Feature", "properties" : {"stage" : i}, "geometry" : {"type": "GeometryCollection", "geometries" : stage_arr}}] )
    return gjsonDict

def write_to_geojson(arr: list, stage: int, stageCount: int, fileName: str):
    resultsFolder = "../placeholder/"
    
    with open(f'{resultsFolder}{fileName}{stage}.json', 'w') as outfile:
        json.dump(create_geojson_dict(arr, stageCount), outfile, indent=4)




idsList = ["kathmandu", "rabat", "marrakesh", "shahalam", "bogota"]
# copenhagen_rail_cq1.json
write_to_geojson(idsList, 4, 1, "yolo")